#include <iostream>
#include <string>
#include <list>
#include <map>

enum enum_scope_type{
    E_GLOBAL,
    E_VARIABLE,
    E_TEMPLATE,
    E_IF,
    E_FOR,
    E_ELEMENT,
    E_SCOPE_LOCAL,
    E_SCOPE_PARAM
};

enum enum_symbol_type{
  T_VAR,
  T_PARAM,
  T_TEMPLATE
};

struct SymbolInfo{
SymbolInfo(std::string _symbol_name, enum_scope_type _type, std::string _scope_name, enum_symbol_type _symbol_type) : symbol_name(_symbol_name), type(_type), scope_name(_scope_name), symbol_type(_symbol_type){}

    std::string symbol_name;
    enum_scope_type type;
    std::string scope_name;
  enum_symbol_type symbol_type; 
};

class SymbolExistsException : public std::exception{
    public:
#ifdef USE_ALL_CPP11_FEATURES
    const char* what() const noexcept override
#else
  const char* what() const noexcept
#endif
  {
    return "Symbol already declared";
  }
};

class PropertyExistsException : public std::exception{
    public:
#ifdef USE_ALL_CPP11_FEATURES
    const char* what() const noexcept override
#else
  const char* what() const noexcept
#endif
  {
    return "Property already declared";
  }
};

class InvalidPropertyException : public std::exception{
    public:
#ifdef USE_ALL_CPP11_FEATURES
    const char* what() const noexcept override
#else
  const char* what() const noexcept
#endif
  {
    return "Property not supported";
  }
};

class XMLTagMismatchedException : public std::exception{
    public:
#ifdef USE_ALL_CPP11_FEATURES
    const char* what() const noexcept override
#else
  const char* what() const noexcept
#endif
  {
    return "Tags not matched";
  }
};

class SymbolTable{
  public:
  ~SymbolTable(){
  }

  void add_symbol(std::string _symbol_name, enum_symbol_type _symbol_type){
    if(is_symbol_and_type_found(_symbol_name, _symbol_type)){
      throw SymbolExistsException();
    }
      symbol_map.insert(std::pair<std::string, enum_symbol_type>(_symbol_name, _symbol_type));
   }

bool is_symbol_and_type_found(const std::string & symbol, enum_symbol_type type) const{
  //  std::cout << "symbol: " << symbol << ' ' << "type: " << type << '\n';
     auto  it = symbol_map.equal_range(symbol);
       for(auto b = it.first; b != it.second; ++b){
	 if(b->second == type){
	   return true;
	 }
       }
       return false;
    }


  private:
  std::multimap<std::string, enum_symbol_type> symbol_map;
};
