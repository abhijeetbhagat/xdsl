
# Copyright 2014 abhijeet bhagat

# xDSL is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# xDSL is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with xDSL.  If not, see <http://www.gnu.org/licenses/>.

override CFLAGS += -Wall

parser.tab.c parser.tab.h: parser.y
	bison -d parser.y

lex.yy.c: tokenizer.l parser.tab.h
	flex tokenizer.l

xDSL: lex.yy.c parser.tab.c parser.tab.h symbol_table_impl.h
	g++ -static-libgcc -static-libstdc++ -std=gnu++0x parser.tab.c lex.yy.c $(CFLAGS) -o xDSL
