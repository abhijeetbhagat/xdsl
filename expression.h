/*
  Copyright 2014 abhijeet bhagat

  xDSL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  xDSL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with xDSL.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <list>
#include <algorithm>
#include <cassert>
#include <iostream>
#include <map>
#include <stack>
#include <unordered_map>
#include "symbol_table_impl.h"

class IChild{
public:
  virtual ~IChild() = 0;
  virtual const std::string render() = 0;
};

inline IChild::~IChild(){}

class IExpression : public IChild{

};

class Symbol{
public:
  Symbol(std::string s) : sym(s){}

  std::string get_sym_name(){
    return sym;
  }
private:
  std::string sym;
};

class Block : public IChild{
public:
  Block(IChild *c){
    children.push_back(c);
    table = new SymbolTable();
    parent = nullptr;
  }

  ~Block(){
    delete table;
    for(auto p : children){
      	delete p;
    }
  }

  virtual const std::string render(){
    for(auto child : children){
      if(child != nullptr){
	output += (child->render());
      }else{

	output += "";
      }
    }

    return output;
  }

  virtual Block* add_child(IChild *child){
    children.push_back(child);
    return this;
  }

  virtual void add_parent(Block *_parent){
    parent = _parent;
  }

  virtual void add_symbol(std::string _symbol_name, enum_symbol_type _symbol_type){
    try{
      table->add_symbol(_symbol_name, _symbol_type); //temp values
    }catch(SymbolExistsException e){
      throw e;
    }
  }

  int get_children_count() const{
    return children.size();
  }

  //Search in the hierarchy
  bool search(const std::string & symbol, enum_symbol_type type){
    return _search(symbol, type, this);
  }

  //Search only in the current scope
  bool search_current_scope(const std::string & symbol, enum_symbol_type type){
    return table->is_symbol_and_type_found(symbol, type);
  }

  void push_on_stack(std::string tag_name) {
    tag_stack.push(tag_name);
  }

  std::string pop_from_stack() {
    std::string top = "";
    if(!tag_stack.empty()){
      top = tag_stack.top();
      tag_stack.pop();
    }
    return top;
  }

  bool all_xml_nodes_ok(){
    auto v =  tag_stack.empty();
    return v;
  }
  
protected:
  bool _search(const std::string & symbol, enum_symbol_type type, Block *b){
    if(b == nullptr)
      return false;
    else if(b->table->is_symbol_and_type_found(symbol, type))
      return true;
    else
      return _search(symbol, type, b->parent);
  }


  Block* parent;
  SymbolTable *table;
  std::list<IChild*> children;
  std::string output;
  std::stack<std::string> tag_stack;
};

// class BoolExpression : IChild{
//  public:
//  BoolExpression(const std::string & e): expr(e){}

//   std::string &render(){
//     return expr;
//   }
//  private:
//   std::string expr;
// };

class IfBlock : public IChild{
public:
  ~IfBlock(){
      delete block;
  }
  IfBlock(std::string e/*BoolExpression *e*/, Block *b) : expr(e), block(b){}

  const std::string render(){
    return "<xsl:if test=\"" + expr + "\">\n" + (block == nullptr ? "" : block->render()) + "</xsl:if>\n";
  }
private:
  //BoolExpression *expr;
  std::string expr;
  Block *block;
};

class WhenBlock : public IChild{
public:
  ~WhenBlock(){
      delete block;
  }
  WhenBlock(std::string e/*BoolExpression *e*/, Block *b) {
    expr=e; 
    block =b;
  }

  const std::string render(){
    //    assert(block == nullptr);
    return "<xsl:when test=\"" + expr + "\">\n" + (block == nullptr ? "" : block->render()) + "</xsl:when>\n";
  }
private:
  //BoolExpression *expr;
  std::string expr;
  Block *block;
};

class ForeachBlock : public IChild{
public:
  ~ForeachBlock(){
      delete block;
  }
  ForeachBlock(std::string _path, Block *b) : path(_path), block(b){}

  const std::string render(){
    return "<xsl:for-each select=\"" + path + "\">\n" + (block == nullptr ? "" : block->render()) + "</xsl:for-each>\n";
  }

private:
  std::string path;
  Block *block;
};


class VariableBlock : public IChild{
public:
  virtual ~VariableBlock(){
      delete block;
  }
  VariableBlock(std::string l, std::string r) : lsym(l), rsym(r), block(nullptr){}
  VariableBlock(std::string l, Block *b):lsym(l), rsym(""), block(b){}
  VariableBlock(std::string l, std::string r, Block *b):lsym(l), rsym(r), containing_block(b){}

  virtual const std::string render(){
    if(containing_block != nullptr){
      const std::string & sym = rsym.substr(1) ;
      if(!(containing_block->search_current_scope(sym, T_VAR)) && !(containing_block->search(sym, T_VAR))){
	std::cout << sym << ": Symbol not found\n";
	throw;
      }
    }
    return "<xsl:variable name=\"" + lsym + "\"" + (block == nullptr ? " select=\""+ rsym + "\" />\n" : ">\n" + block->render() + "</xsl:variable>\n");
  }

protected:
  std::string lsym, rsym;
  Symbol *sym = nullptr;
  Block *block = nullptr ;
  Block *containing_block = nullptr;

};

class ParamBlock : public VariableBlock{
public:

  ParamBlock(std::string l, std::string r) : VariableBlock(l, r){}
  ParamBlock(std::string l, Block *b):VariableBlock(l, b){}
  ParamBlock(std::string l, std::string r, Block *b):VariableBlock(l, r, b){}

  const std::string render() override{
    return "<xsl:param name=\"" + lsym + "\"" + (block == nullptr ? " select=\""+ rsym + "\" />\n" : ">\n" + block->render() + "</xsl:param>\n");
  }
};

class PropertyDeclaration{
public:
  PropertyDeclaration(std::string n, std::string v) : name(n), value(v){}

  std::string get_key(){return name;}
  std::string get_value(){return value;}

private:
  std::string name, value;
};

class PropertyDeclarationList{
public:
  PropertyDeclarationList(PropertyDeclaration *decl) {
    kv_store.insert(std::pair<std::string, std::string>(decl->get_key(), decl->get_value()));
  }

  virtual PropertyDeclarationList* add_property(PropertyDeclaration *decl){
    auto it = kv_store.insert(std::pair<std::string, std::string>(decl->get_key(), decl->get_value()));
    if(it.second == false){
      throw PropertyExistsException();
    }
    return this;
  }

  virtual std::string translate(){
    std::string str;
    for(auto & p : kv_store){
      str += p.first + "=" + "\"" + p.second + "\" ";
    }
    return str;
  }

  std::unordered_map<std::string, std::string>::iterator begin(){
    return kv_store.begin();
  }

  std::unordered_map<std::string, std::string>::iterator end(){
    return kv_store.end();
  }

protected:
  std::unordered_map<std::string, std::string> kv_store;
};

class ArgumentDeclarationList : public PropertyDeclarationList{
public:
  ArgumentDeclarationList(PropertyDeclaration *decl) : PropertyDeclarationList(decl){}

  std::string translate(){
    std::string str="";
    for(auto & p : kv_store){
      str += "<xsl:param name=\"" + p.first + "\" select=\"" + p.second + "\" />\n";
    }
    return str;
  }
};

class WithParamDeclarationList : public PropertyDeclarationList{
public:
  WithParamDeclarationList(PropertyDeclaration *decl) : PropertyDeclarationList(decl){}
  std::string translate(){
    std::string str="";
    for(auto & p : kv_store){
      str += "<xsl:with-param name=\"" + p.first + "\" select=\"" + p.second + "\" />\n";
    }
    return str;
  }
};

class TemplateDeclaration : public IChild{
public:
  ~TemplateDeclaration(){
      delete block;
  }
  TemplateDeclaration(PropertyDeclarationList *dl,
		      PropertyDeclarationList *al,
		      Block *b) : prop_list(dl), arg_list(al), block(b){
    for(auto b = dl->begin(); b != dl->end(); ++b){
      if((b->first != "name") && (b->first != "match") && (b->first != "priority") && (b->first != "mode")){
	throw InvalidPropertyException();
      }
    }
  }

  const std::string render(){
    return "<xsl:template " + prop_list->translate() + ">\n" + (arg_list == nullptr ? "" : arg_list->translate())+ (block == nullptr ? "" : block->render()) + "</xsl:template>\n";
  }

private:

  PropertyDeclarationList *prop_list, *arg_list;

  Block *block;
};

class RHSValue{
public:
  RHSValue(std::string v) : value (v){}
  std::string get_value(){
    return value;
  }
private:
  std::string value;
};

class StylesheetDeclaration : public IChild{
public:
  ~StylesheetDeclaration(){
      delete block;
  }
  StylesheetDeclaration(Block *b) : block(b){}

  const std::string render(){
    return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n" + (block == nullptr ? "" : block->render()) + "</xsl:stylesheet>";
  }

private:
  Block *block;
};

class CaseBlock : public IChild{
public:
  ~CaseBlock(){
      delete block;
  }
  CaseBlock(Block* b) : block(b){}

  const std::string render(){
    return "<xsl:choose>\n" + (block == nullptr ? "" : block->render()) + "</xsl:choose>\n";
  }

private:
  Block *block;
};

class OtherwiseBlock : public IChild{
public:
  ~OtherwiseBlock(){
      delete block;
  }
  OtherwiseBlock(Block *b) : block(b){}

  const std::string render(){
    return "<xsl:otherwise>\n" + (block == nullptr ? "" : block->render()) + "</xsl:otherwise>\n";
  }

private:
  Block *block;
};

class ElementBlock : public IChild{
public:
  ~ElementBlock(){
      delete block;
  }
  ElementBlock(PropertyDeclarationList *_own_attr_list, PropertyDeclarationList *_element_attr_list, Block *b) : own_attr_list(_own_attr_list), element_attr_list(_element_attr_list), block(b){}

  const std::string render(){
    std::string e = "<xsl:element " + own_attr_list->translate() + ">\n";
    std::string attr_list = "";
    if(element_attr_list != nullptr){
      for(auto b = element_attr_list->begin(); b != element_attr_list->end(); ++b){
	attr_list += "<xsl:attribute name=\"" + b->first + "\">" + b->second + "</xsl:attribute>\n";
      }
    }
    return e + attr_list + (block == nullptr ? "" : block->render()) + "</xsl:element>\n";
  }
private:
  PropertyDeclarationList *own_attr_list, *element_attr_list;
  Block *block;
};

class Attribute : public IChild{
public:
  ~Attribute(){
      delete block;
  }
  Attribute(std::string name, Block *b) : attr_name(name), block(b){}


  const std::string render(){
    return "<xsl:attribute name=\"" + attr_name + "\">" +(block == nullptr ? "" : block->render()) + "</xsl:attribute>\n";
  }
private:
  std::string attr_name;
  Block* block;
};

class StrayText : public IChild{
public:
  StrayText(std::string txt) : text(txt){}

  const std::string render(){
    return text;
  }

private:
  std::string text;
};


class Text : public IChild{
public:
  Text(bool disable_output_escaping, const std::string & text): disable(disable_output_escaping), content(text){}

  const std::string render(){
    std::string prop = (disable == true ? " disable-output-escaping = \"yes\"" : "");
    return "<xsl:text"+ prop  +">" + content + "</xsl:text>\n";
  }
private:
  bool disable;
  std::string content;

};

class TemplateCall : public IChild{
public:
  ~TemplateCall(){
      delete arg_children;
  }
  TemplateCall(const std::string & name, PropertyDeclarationList *decl_list, Block *root_ptr, Block *args) : temp_name(name), param_list(decl_list), root_block(root_ptr), arg_children(args){}

  const std::string render(){
    //do a call validation here using root_ptr
    if(!root_block->search(temp_name, T_TEMPLATE)){
      std::cout << temp_name << ": not found.\n";
      throw ;
    }
    return "<xsl:call-template name=\"" + temp_name + "\">\n" + (param_list == nullptr ? "" : param_list->translate()) + (arg_children == nullptr ? "" : arg_children->render()) + "</xsl:call-template>\n";
  }
private:
  std::string temp_name;
  PropertyDeclarationList *param_list;
  Block *root_block;
  Block *arg_children;
};

class Copy : public IChild{
public:
  ~Copy(){
      delete block;
  }
  Copy(PropertyDeclaration *_property, Block *b) : property(_property), block(b){}

  const std::string render(){
    return "<xsl:copy " + (property == nullptr ? "" : property->get_key() + "=\"" + property->get_value() + "\"") + (block == nullptr ? "/>\n" : ">\n" + block->render() + "</xsl:copy>\n");
  }
private:
  PropertyDeclaration *property;
  Block *block;
};

class CopyOf : public IChild{
public:
  CopyOf(const std::string & _expression) : expression(_expression){}

  const std::string render(){
    return "<xsl:copy-of select=\"" + expression + "\" />\n";
  }

private:
  std::string expression;
};

class WithParamBlock : public IChild{
public:
  ~WithParamBlock(){
      delete block;
  }
  WithParamBlock(const std::string & arg_name, Block *b) : name(arg_name), block(b) {}

  const std::string render(){
    return "<xsl:with-param name=\">" + name + "\">\n" + (block == nullptr ? "" : block->render()) + "</xsl:with-param>\n";
  }
private:
  std::string name;
  Block *block;
};

class ImportDeclaration : public IChild{
public:
  ImportDeclaration(const std::string & _uri) : uri(_uri){}

  const std::string render(){
    return "<xsl:import href = \"" + uri + "\">\n";
  }
private:
  std::string uri;
};

class XMLTag : public IChild{
public:
  XMLTag(const std::string & name, bool tag_type) : tag_name(name), is_start_tag(tag_type){}

  const std::string render(){
    if(is_start_tag)
      return "<" + tag_name + ">\n";
    else
      return "</" + tag_name + ">\n";
  }

  const std::string & get_tag_name(){
    return tag_name;
  }
private:
  std::string tag_name;
  bool is_start_tag;
};

class Output : public IChild{
public:
  Output(PropertyDeclarationList *dl) : prop_list(dl){
    for(auto b = dl->begin(); b != dl->end(); ++b){
      if((b->first != "method") && 
	 (b->first != "version") && 
	 (b->first != "encoding") && 
	 (b->first != "omit-xml-declaration") && 
	 (b->first != "standalone")  &&
	 (b->first != "doctype-public") &&
	 (b->first != "doctype-system") &&
	 (b->first != "cdata-section-elements") &&
	 (b->first != "indent") &&
	 (b->first != "media-type")
	 ){
	throw InvalidPropertyException();
      }
    }
  }  

 const std::string render(){
   return "<xsl:output " + prop_list->translate() + " />\n" ;
  }
private:
  PropertyDeclarationList *prop_list;
};

class ScriptBlock : public IChild{
public:
  ScriptBlock(PropertyDeclarationList *_dl, const std::string & _code) : dl(_dl), code(_code){}

  const std::string render(){
   return "<xsl:script " + dl->translate() + ">\n<![CDATA[\n " + code + " \n]]>\n</xsl:script>\n" ;
  }
private:
  PropertyDeclarationList *dl;
  std::string code;
};
