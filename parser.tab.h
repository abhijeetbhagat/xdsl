/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_PARSER_TAB_H_INCLUDED
# define YY_YY_PARSER_TAB_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     STRING = 258,
     PROPERTY_NAME = 259,
     CODE = 260,
     STRAY = 261,
     SYM = 262,
     ATTR = 263,
     HIPHENATED_SYM = 264,
     PLUS = 265,
     MINUS = 266,
     MUL = 267,
     DIVIDE = 268,
     NL = 269,
     SS = 270,
     IN = 271,
     TEMPLATE = 272,
     VAR = 273,
     DOUBLE_QUOTE = 274,
     IF = 275,
     FOR_EACH = 276,
     LOGICAL_OR = 277,
     LOGICAL_AND = 278,
     LOGICAL_COMP = 279,
     CASE = 280,
     WHEN = 281,
     ELSE = 282,
     ELEMENT = 283,
     IMPORT = 284,
     TEXT = 285,
     CALL = 286,
     COPY = 287,
     COPY_OF = 288,
     OUTPUT = 289,
     ARG = 290,
     SCRIPT = 291,
     START_TAG = 292,
     END_TAG = 293
   };
#endif


#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
/* Line 2058 of yacc.c  */
#line 43 "parser.y"

  string *str;
  Symbol *sym;
  //StylesheetDeclaration *stylesheet_decl;
  //TemplateDeclaration *temp_decl;
  PropertyDeclaration *prop_decl;
  PropertyDeclarationList *prop_list_decl;
  ArgumentDeclarationList *arg_list_decl;
  WithParamDeclarationList *with_param_list_decl;
  //  BoolExpression *bool_expr;
  RHSValue *rhs_value;
  //IfBlock *if_block;
  IChild *child;
  //ForeachBlock *for_block;
  //CaseBlock *case_block;
  //ElementBlock *element_block;
  Block *block;


/* Line 2058 of yacc.c  */
#line 115 "parser.tab.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;
extern YYLTYPE yylloc;
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_PARSER_TAB_H_INCLUDED  */
