/*
    Copyright 2014 abhijeet bhagat

    xDSL is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    xDSL is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with xDSL.  If not, see <http://www.gnu.org/licenses/>.
*/

%{
#include <iostream>
#include "expression.h"
using namespace std;
#define YY_DECL extern "C" int yylex()
#include "parser.tab.h"
 int yycolumn = 1;
#define YY_USER_ACTION {yylloc.first_line = yylineno; \
               yylloc.last_line = yylineno; \
               yylloc.first_column = yycolumn; yylloc.last_column = yycolumn + yyleng - 1;\
               yycolumn += yyleng; \
                }
%}

%option yylineno

Letter   [A-Za-z]
Digit    [0-9]
NameChar {Letter}|{Digit}|[-._:]
Name     ({Letter}|[_:]){NameChar}*

%%
"<<"(.|[\n])+">>"          {
                             std::string symbol = yytext;
                             yylval.str = new string(symbol.substr(2, symbol.length() - 4)); return CODE;
                           }
[ \t]+             ;
\n                 {yycolumn = 1;}

"<"{Name}">"       {
                     std::string symbol = yytext;
                     yylval.sym = new Symbol(symbol.substr(1, symbol.length() - 2)); return START_TAG;
	           }
"</"{Name}">"      {
                       std::string symbol = yytext;
		       yylval.sym = new Symbol(symbol.substr(2, symbol.length() - 3)); return END_TAG;
	           }
"ss"               return SS;
"in"               return IN;
"var"              return VAR;
"template"         return TEMPLATE;
"if"               return IF;
"foreach"          return FOR_EACH;
"case"             return CASE;
"when"             return WHEN;
"else"             return ELSE;
"elem"             return ELEMENT;
"text"             return TEXT;
"call"             return CALL;
"cp"               return COPY;
"cpof"             return COPY_OF;
"arg"              return ARG;
"import"           return IMPORT;
"op"               return OUTPUT;
"script"           return SCRIPT;
@[a-zA-Z]+         {yylval.sym = new Symbol(std::string(yytext).substr(1)); return ATTR;}
"+"                return PLUS;
"-"                return MINUS;
"*"                return MUL;
";"                return ';';
":"                return ':';
","                return ',';
"="                return '=';
"("                return '(';
")"                return ')';
"{"                return '{';
"}"                return '}';
"["                return '[';
"]"                return ']';
"||"|"or"          return LOGICAL_OR;
"&&"|"and"         return LOGICAL_AND;
"=="               return LOGICAL_COMP;
"`"                return '`';
\"(\\.|[^\\"])*\"  {
                    std::string text = yytext;
                    if(text.length() == 2){
                     yylval.str = new string("''");
                    }else{
                     yylval.str = new string(text.substr(1, text.length()-2));
                    }
                    return STRING;
                   }
[a-zA-Z]+(\-[a-zA-Z]+)+        {yylval.sym = new Symbol(yytext); return HIPHENATED_SYM;}
[a-zA-Z]+          {yylval.sym = new Symbol(yytext); return SYM;}
"`"([^&]|[\n])+"`"    { std::string symbol = yytext;
                             yylval.str = new string(symbol.substr(2, symbol.length() - 3)); return STRAY;}
%%

int yywrap() { return 1; }
