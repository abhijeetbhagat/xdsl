/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.7"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
/* Line 371 of yacc.c  */
#line 18 "parser.y"

#include <iostream>
#include <cstdio>
#include "expression.h"
#include <cassert>

using namespace std;

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
 extern int yylineno;
 extern char *yytext;

void yyerror(const char *s);

Block *current_block_ptr = nullptr;
Block *root_ptr = nullptr;
char last_template_child_type;
std::stack<Block*> scopes_stack;
//std::stack<Block*> scopes_stack;

/* Line 371 of yacc.c  */
#line 91 "parser.tab.c"

# ifndef YY_NULL
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULL nullptr
#  else
#   define YY_NULL 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "parser.tab.h".  */
#ifndef YY_YY_PARSER_TAB_H_INCLUDED
# define YY_YY_PARSER_TAB_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     STRING = 258,
     PROPERTY_NAME = 259,
     CODE = 260,
     STRAY = 261,
     SYM = 262,
     ATTR = 263,
     HIPHENATED_SYM = 264,
     PLUS = 265,
     MINUS = 266,
     MUL = 267,
     DIVIDE = 268,
     NL = 269,
     SS = 270,
     IN = 271,
     TEMPLATE = 272,
     VAR = 273,
     DOUBLE_QUOTE = 274,
     IF = 275,
     FOR_EACH = 276,
     LOGICAL_OR = 277,
     LOGICAL_AND = 278,
     LOGICAL_COMP = 279,
     CASE = 280,
     WHEN = 281,
     ELSE = 282,
     ELEMENT = 283,
     IMPORT = 284,
     TEXT = 285,
     CALL = 286,
     COPY = 287,
     COPY_OF = 288,
     OUTPUT = 289,
     ARG = 290,
     SCRIPT = 291,
     START_TAG = 292,
     END_TAG = 293
   };
#endif


#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
/* Line 387 of yacc.c  */
#line 43 "parser.y"

  string *str;
  Symbol *sym;
  //StylesheetDeclaration *stylesheet_decl;
  //TemplateDeclaration *temp_decl;
  PropertyDeclaration *prop_decl;
  PropertyDeclarationList *prop_list_decl;
  ArgumentDeclarationList *arg_list_decl;
  WithParamDeclarationList *with_param_list_decl;
  //  BoolExpression *bool_expr;
  RHSValue *rhs_value;
  //IfBlock *if_block;
  IChild *child;
  //ForeachBlock *for_block;
  //CaseBlock *case_block;
  //ElementBlock *element_block;
  Block *block;


/* Line 387 of yacc.c  */
#line 192 "parser.tab.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;
extern YYLTYPE yylloc;
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_PARSER_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

/* Line 390 of yacc.c  */
#line 233 "parser.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(N) (N)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
	     && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   639

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  50
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  63
/* YYNRULES -- Number of rules.  */
#define YYNRULES  183
/* YYNRULES -- Number of states.  */
#define YYNSTATES  306

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   293

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      20,    21,     2,     2,    28,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    29,    14,
       2,    16,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    38,     2,    39,     2,     2,    40,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    26,     2,    27,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    15,
      17,    18,    19,    22,    23,    24,    25,    30,    31,    32,
      33,    34,    35,    36,    37,    41,    42,    43,    44,    45,
      46,    47,    48,    49
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     4,     5,     9,    12,    16,    18,    21,
      23,    25,    27,    29,    31,    33,    35,    37,    39,    43,
      49,    55,    59,    60,    67,    69,    72,    74,    76,    78,
      80,    82,    84,    86,    88,    94,   100,   104,   105,   112,
     114,   117,   119,   121,   123,   125,   127,   129,   131,   133,
     138,   142,   147,   154,   162,   164,   167,   169,   171,   173,
     175,   177,   179,   181,   183,   185,   187,   189,   191,   193,
     195,   197,   201,   205,   209,   213,   215,   219,   221,   225,
     230,   234,   239,   244,   250,   252,   255,   257,   259,   261,
     263,   265,   267,   269,   274,   278,   283,   285,   288,   290,
     292,   294,   296,   298,   300,   306,   309,   311,   314,   319,
     323,   328,   330,   333,   335,   337,   339,   341,   343,   345,
     347,   348,   352,   357,   359,   362,   364,   366,   368,   370,
     372,   374,   384,   395,   403,   410,   412,   415,   417,   419,
     421,   423,   425,   430,   432,   435,   437,   439,   441,   443,
     445,   447,   452,   455,   463,   470,   476,   482,   484,   487,
     495,   497,   500,   502,   504,   506,   508,   510,   517,   525,
     529,   534,   536,   539,   541,   543,   545,   547,   549,   555,
     561,   563,   565,   571
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      51,     0,    -1,    -1,    -1,    17,    52,    53,    -1,    26,
      27,    -1,    26,    54,    27,    -1,    55,    -1,    54,    55,
      -1,    57,    -1,    61,    -1,    66,    -1,    56,    -1,    98,
      -1,   111,    -1,   112,    -1,   109,    -1,   110,    -1,    37,
       3,    14,    -1,    18,     7,    16,     7,    14,    -1,    18,
       7,    16,     3,    14,    -1,    18,     7,    14,    -1,    -1,
      18,     7,    26,    58,    59,    27,    -1,    60,    -1,    59,
      60,    -1,    61,    -1,    74,    -1,    78,    -1,    81,    -1,
      92,    -1,    99,    -1,   109,    -1,   110,    -1,    22,     7,
      16,     7,    14,    -1,    22,     7,    16,     3,    14,    -1,
      22,     7,    14,    -1,    -1,    22,     7,    26,    62,    63,
      27,    -1,    64,    -1,    63,    64,    -1,    61,    -1,    74,
      -1,    78,    -1,    81,    -1,    92,    -1,    99,    -1,   109,
      -1,   110,    -1,    19,    38,    69,    39,    -1,    65,    26,
      27,    -1,    65,    26,    67,    27,    -1,    65,    20,    71,
      21,    26,    27,    -1,    65,    20,    71,    21,    26,    67,
      27,    -1,    68,    -1,    67,    68,    -1,    57,    -1,    61,
      -1,    74,    -1,    78,    -1,    81,    -1,    92,    -1,    99,
      -1,   105,    -1,   108,    -1,   100,    -1,    82,    -1,    98,
      -1,   109,    -1,   110,    -1,    70,    -1,    69,    28,    70,
      -1,     7,    29,     7,    -1,     9,    29,     7,    -1,     7,
      29,     3,    -1,    70,    -1,    71,    28,    70,    -1,    70,
      -1,    72,    28,    70,    -1,    24,    20,     3,    21,    -1,
      73,    26,    27,    -1,    73,    26,    75,    27,    -1,    73,
      26,    27,    89,    -1,    73,    26,    75,    27,    89,    -1,
      76,    -1,    75,    76,    -1,    61,    -1,    78,    -1,    81,
      -1,    74,    -1,   100,    -1,   109,    -1,   110,    -1,    25,
      20,     3,    21,    -1,    77,    26,    27,    -1,    77,    26,
      79,    27,    -1,    80,    -1,    79,    80,    -1,    61,    -1,
      78,    -1,    81,    -1,    74,    -1,   109,    -1,   110,    -1,
      33,    26,    83,    89,    27,    -1,    85,    89,    -1,    85,
      -1,    83,    85,    -1,    34,    20,     3,    21,    -1,    84,
      26,    27,    -1,    84,    26,    86,    27,    -1,    87,    -1,
      86,    87,    -1,    61,    -1,    74,    -1,    78,    -1,    81,
      -1,   109,    -1,   110,    -1,    35,    -1,    -1,    88,    26,
      27,    -1,    88,    26,    90,    27,    -1,    91,    -1,    90,
      91,    -1,    61,    -1,    74,    -1,    78,    -1,    81,    -1,
     109,    -1,   110,    -1,    36,    38,    69,    39,    20,    69,
      21,    26,    27,    -1,    36,    38,    69,    39,    20,    69,
      21,    26,    93,    27,    -1,    36,    38,    69,    39,    26,
      93,    27,    -1,    36,    38,    69,    39,    26,    27,    -1,
      94,    -1,    93,    94,    -1,    61,    -1,    74,    -1,    78,
      -1,    81,    -1,    95,    -1,     8,    26,    96,    27,    -1,
      97,    -1,    96,    97,    -1,    61,    -1,    74,    -1,    78,
      -1,    81,    -1,    98,    -1,     6,    -1,    41,    16,    98,
      14,    -1,    41,    14,    -1,    41,    38,    70,    39,    16,
      98,    14,    -1,    42,     7,    20,    72,    21,    14,    -1,
      42,     7,    20,    21,    14,    -1,    42,     7,    26,   101,
      27,    -1,   102,    -1,   101,   102,    -1,    46,    38,     7,
      39,    26,   103,    27,    -1,   104,    -1,   103,   104,    -1,
      61,    -1,    74,    -1,    78,    -1,    81,    -1,    95,    -1,
      43,    38,    70,    39,    26,    27,    -1,    43,    38,    70,
      39,    26,   106,    27,    -1,    43,    26,    27,    -1,    43,
      26,   106,    27,    -1,   107,    -1,   106,   107,    -1,    61,
      -1,    74,    -1,    78,    -1,    81,    -1,    95,    -1,    44,
      20,     7,    21,    14,    -1,    44,    20,     3,    21,    14,
      -1,    48,    -1,    49,    -1,    45,    38,    69,    39,    14,
      -1,    47,    38,    69,    39,    26,     5,    27,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   174,   174,   175,   175,   189,   192,   201,   206,   210,
     211,   212,   213,   214,   215,   216,   217,   218,   233,   237,
     250,   261,   273,   272,   300,   308,   312,   313,   314,   315,
     316,   317,   318,   319,   336,   349,   360,   372,   371,   397,
     405,   409,   410,   411,   412,   413,   414,   415,   416,   432,
     453,   463,   483,   496,   508,   519,   543,   544,   545,   546,
     547,   548,   549,   550,   551,   552,   553,   554,   555,   556,
     572,   573,   577,   578,   579,   582,   583,   586,   587,   600,
     611,   612,   621,   627,   637,   647,   651,   652,   653,   654,
     655,   656,   657,   679,   691,   692,   703,   707,   710,   711,
     712,   713,   714,   715,   732,   744,   754,   755,   758,   768,
     771,   783,   787,   790,   791,   792,   793,   794,   795,   811,
     820,   822,   825,   836,   840,   843,   844,   845,   846,   847,
     848,   865,   866,   870,   874,   881,   887,   891,   892,   893,
     894,   895,   898,   902,   908,   912,   913,   914,   915,   916,
     919,   922,   923,   924,   936,   940,   944,   950,   957,   961,
     976,   983,   987,   988,   989,   990,   991,   995,   996,   997,
     998,  1001,  1007,  1011,  1012,  1013,  1014,  1015,  1018,  1027,
    1030,  1031,  1033,  1038
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "STRING", "PROPERTY_NAME", "CODE",
  "STRAY", "SYM", "ATTR", "HIPHENATED_SYM", "PLUS", "MINUS", "MUL",
  "DIVIDE", "';'", "NL", "'='", "SS", "IN", "TEMPLATE", "'('", "')'",
  "VAR", "DOUBLE_QUOTE", "IF", "FOR_EACH", "'{'", "'}'", "','", "':'",
  "LOGICAL_OR", "LOGICAL_AND", "LOGICAL_COMP", "CASE", "WHEN", "ELSE",
  "ELEMENT", "IMPORT", "'['", "']'", "'`'", "TEXT", "CALL", "COPY",
  "COPY_OF", "OUTPUT", "ARG", "SCRIPT", "START_TAG", "END_TAG", "$accept",
  "stylesheet", "$@1", "stylesheet_decl", "stylesheet_children",
  "stylesheet_child", "import_decl", "param_block", "$@2",
  "param_children", "param_child", "var_block", "$@3", "var_children",
  "var_child", "template_header", "template_decl", "temp_children",
  "temp_child", "property_comma_seperated_list", "property_decl",
  "argument_comma_seperated_list", "with_param_comma_seperated_list",
  "if_start", "if_block", "if_children", "if_child", "for_start",
  "for_block", "for_children", "for_child", "case_block", "when_else",
  "when_list", "when_start", "when_clause", "when_children", "when_child",
  "else_start", "else_clause", "else_children", "else_child",
  "element_block", "element_children", "element_child", "attribute_block",
  "attribute_children", "attribute_child", "straytext", "text_node",
  "template_call", "arg_decls", "arg_decl", "arg_children", "arg_child",
  "copy", "copy_children", "copy_child", "copy_of", "xml_start_tag",
  "xml_end_tag", "output", "script_block", YY_NULL
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,    59,   269,    61,   270,   271,   272,
      40,    41,   273,   274,   275,   276,   123,   125,    44,    58,
     277,   278,   279,   280,   281,   282,   283,   284,    91,    93,
      96,   285,   286,   287,   288,   289,   290,   291,   292,   293
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    50,    51,    52,    51,    53,    53,    54,    54,    55,
      55,    55,    55,    55,    55,    55,    55,    55,    56,    57,
      57,    57,    58,    57,    59,    59,    60,    60,    60,    60,
      60,    60,    60,    60,    61,    61,    61,    62,    61,    63,
      63,    64,    64,    64,    64,    64,    64,    64,    64,    65,
      66,    66,    66,    66,    67,    67,    68,    68,    68,    68,
      68,    68,    68,    68,    68,    68,    68,    68,    68,    68,
      69,    69,    70,    70,    70,    71,    71,    72,    72,    73,
      74,    74,    74,    74,    75,    75,    76,    76,    76,    76,
      76,    76,    76,    77,    78,    78,    79,    79,    80,    80,
      80,    80,    80,    80,    81,    82,    83,    83,    84,    85,
      85,    86,    86,    87,    87,    87,    87,    87,    87,    88,
      89,    89,    89,    90,    90,    91,    91,    91,    91,    91,
      91,    92,    92,    92,    92,    93,    93,    94,    94,    94,
      94,    94,    95,    96,    96,    97,    97,    97,    97,    97,
      98,    99,    99,    99,   100,   100,   100,   101,   101,   102,
     103,   103,   104,   104,   104,   104,   104,   105,   105,   105,
     105,   106,   106,   107,   107,   107,   107,   107,   108,   108,
     109,   110,   111,   112
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     0,     3,     2,     3,     1,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     3,     5,
       5,     3,     0,     6,     1,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     5,     5,     3,     0,     6,     1,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     4,
       3,     4,     6,     7,     1,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     3,     3,     3,     3,     1,     3,     1,     3,     4,
       3,     4,     4,     5,     1,     2,     1,     1,     1,     1,
       1,     1,     1,     4,     3,     4,     1,     2,     1,     1,
       1,     1,     1,     1,     5,     2,     1,     2,     4,     3,
       4,     1,     2,     1,     1,     1,     1,     1,     1,     1,
       0,     3,     4,     1,     2,     1,     1,     1,     1,     1,
       1,     9,    10,     7,     6,     1,     2,     1,     1,     1,
       1,     1,     4,     1,     2,     1,     1,     1,     1,     1,
       1,     4,     2,     7,     6,     5,     5,     1,     2,     7,
       1,     2,     1,     1,     1,     1,     1,     6,     7,     3,
       4,     1,     2,     1,     1,     1,     1,     1,     5,     5,
       1,     1,     5,     7
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       2,     3,     0,     0,     1,     0,     4,   150,     0,     0,
       0,     5,     0,     0,     0,   180,   181,     0,     7,    12,
       9,    10,     0,    11,    13,    16,    17,    14,    15,     0,
       0,     0,     0,     0,     0,     6,     8,     0,     0,    21,
       0,    22,     0,     0,     0,    70,    36,     0,    37,    18,
       0,     0,    75,     0,     0,     0,    50,     0,     0,     0,
       0,     0,     0,     0,    56,    57,     0,    54,     0,    58,
       0,    59,    60,    66,     0,   120,    61,    67,    62,    65,
      63,    64,    68,    69,     0,     0,     0,     0,     0,     0,
      49,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   152,     0,     0,     0,     0,     0,     0,
      51,    55,     0,     0,     0,   119,     0,   105,    20,    19,
       0,    24,    26,    27,    28,    29,    30,    31,    32,    33,
      74,    72,    73,    71,    35,    34,    41,     0,    39,    42,
      43,    44,    45,    46,    47,    48,   182,     0,     0,    76,
       0,     0,   120,   106,     0,     0,     0,     0,     0,     0,
       0,   169,   173,   174,   175,   176,   177,     0,   171,     0,
       0,     0,    80,    86,    89,     0,    84,    87,    88,    90,
      91,    92,    94,    98,   101,    99,     0,    96,   100,   102,
     103,   109,   113,   114,   115,   116,     0,   111,   117,   118,
       0,    23,    25,    38,    40,     0,    52,     0,    79,    93,
     107,     0,   108,     0,   151,     0,     0,    77,     0,     0,
       0,   157,     0,   170,   172,     0,     0,     0,    82,    81,
      85,    95,    97,   110,   112,   121,   125,   126,   127,   128,
       0,   123,   129,   130,   183,    53,   104,     0,     0,     0,
     155,     0,     0,     0,   156,   158,   145,   146,   147,   148,
       0,   143,   149,     0,   179,   178,    83,   122,   124,     0,
     134,   137,   138,   139,   140,     0,   135,   141,     0,   154,
      78,     0,   142,   144,   167,     0,     0,   133,   136,   153,
       0,   168,     0,     0,   131,     0,   162,   163,   164,   165,
     166,     0,   160,   132,   159,   161
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     3,     6,    17,    18,    19,    64,    86,   120,
     121,    65,    93,   137,   138,    22,    23,    66,    67,    44,
      45,    53,   218,    68,    69,   175,   176,    70,    71,   186,
     187,    72,    73,   152,    74,    75,   196,   197,   116,   117,
     240,   241,    76,   275,   276,   166,   260,   261,    77,    78,
      79,   220,   221,   301,   302,    80,   167,   168,    81,    82,
      83,    27,    28
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -267
static const yytype_int16 yypact[] =
{
       6,  -267,    39,    21,  -267,   120,  -267,  -267,    87,    61,
     130,  -267,   157,   140,   149,  -267,  -267,   127,  -267,  -267,
    -267,  -267,    60,  -267,  -267,  -267,  -267,  -267,  -267,    79,
      12,   103,   144,    12,    12,  -267,  -267,    12,   404,  -267,
      19,  -267,   142,   163,    34,  -267,  -267,   133,  -267,  -267,
      44,    88,  -267,    35,   188,   193,  -267,   189,   196,   185,
      32,   218,   118,   210,  -267,  -267,   445,  -267,   206,  -267,
     207,  -267,  -267,  -267,   208,   201,  -267,  -267,  -267,  -267,
    -267,  -267,  -267,  -267,   225,   226,   261,   182,   234,    12,
    -267,   228,   235,   261,   240,   222,   233,    12,   257,   259,
     231,   264,    12,  -267,   262,    12,   170,   239,    12,   202,
    -267,  -267,   204,    16,   155,  -267,   243,  -267,  -267,  -267,
       9,  -267,  -267,  -267,  -267,  -267,  -267,  -267,  -267,  -267,
    -267,  -267,  -267,  -267,  -267,  -267,  -267,    65,  -267,  -267,
    -267,  -267,  -267,  -267,  -267,  -267,  -267,   268,   484,  -267,
     254,   258,    86,  -267,   260,   102,   275,   252,    23,   246,
     269,  -267,  -267,  -267,  -267,  -267,  -267,   526,  -267,   265,
     277,   278,   201,  -267,  -267,   293,  -267,  -267,  -267,  -267,
    -267,  -267,  -267,  -267,  -267,  -267,   297,  -267,  -267,  -267,
    -267,  -267,  -267,  -267,  -267,  -267,   329,  -267,  -267,  -267,
     341,  -267,  -267,  -267,  -267,   273,  -267,   513,  -267,  -267,
    -267,   281,  -267,   186,  -267,   289,   298,  -267,    75,   276,
      -9,  -267,   177,  -267,  -267,   285,   302,   309,  -267,   201,
    -267,  -267,  -267,  -267,  -267,  -267,  -267,  -267,  -267,  -267,
     449,  -267,  -267,  -267,  -267,  -267,  -267,    12,   544,   262,
    -267,   311,    12,   321,  -267,  -267,  -267,  -267,  -267,  -267,
     128,  -267,  -267,   556,  -267,  -267,  -267,  -267,  -267,   138,
    -267,  -267,  -267,  -267,  -267,   566,  -267,  -267,   322,  -267,
    -267,   299,  -267,  -267,  -267,   576,   317,  -267,  -267,  -267,
     318,  -267,   586,    52,  -267,   596,  -267,  -267,  -267,  -267,
    -267,   606,  -267,  -267,  -267,  -267
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -267,  -267,  -267,  -267,  -267,   320,  -267,   146,  -267,  -267,
     229,    -5,  -267,  -267,   211,  -267,  -267,   213,   -64,   -23,
     -30,  -267,  -267,  -267,   107,  -267,   183,  -267,   164,  -267,
     166,   220,  -267,  -267,  -267,   -86,  -267,   173,  -267,  -147,
    -267,   119,   -69,    80,  -266,  -240,  -267,   111,    -4,   -66,
    -106,  -267,   153,  -267,    74,  -267,   113,  -163,  -267,    -2,
      11,  -267,  -267
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint16 yytable[] =
{
      21,    24,   111,    25,   224,   211,   179,    52,   277,   288,
      50,    51,    21,    24,   153,    25,    26,   126,   254,    42,
     127,    43,    84,     1,   142,   228,    85,   143,    26,   288,
      42,    10,    43,    54,    55,   277,   201,   219,    10,     4,
      54,    55,    57,   182,   216,    59,   103,     5,   104,    57,
      60,   126,   277,   300,   127,   277,    96,    15,    16,   133,
     160,   300,    89,    97,    15,    16,   210,   149,   142,   179,
     105,   143,    89,    90,    10,   157,    54,    55,   169,   155,
      37,   122,   266,    94,   128,    57,    38,    10,   136,    54,
      55,   144,   203,    39,    29,    40,   251,   129,    57,    30,
     156,    59,   162,   252,   145,    41,    60,   173,   183,   192,
     180,   189,   198,    15,    16,   122,    89,    46,   128,    47,
      58,   115,   224,   181,   190,   199,     7,    95,   217,    48,
      89,   129,   136,     7,     7,   144,    91,    31,     8,     9,
      92,   213,    10,   111,   107,     8,     9,    11,   145,    10,
      10,    20,    54,    55,    35,   282,   108,    12,    49,   286,
      32,    57,   162,    20,    12,    13,    89,    14,    15,    16,
     173,    87,    13,   180,    14,    15,    16,    10,    33,    54,
      55,   183,   191,     7,   189,   130,   181,    34,    57,   131,
     158,   192,    88,   123,   198,   236,   159,   190,   242,    10,
     139,    54,    55,    15,    16,   170,   247,   199,    98,   171,
      57,   243,   248,    99,   163,   100,   101,   256,   262,   174,
     184,   193,   280,   102,   269,   106,    10,   123,    54,    55,
     109,   172,   112,   113,   114,   236,   115,    57,   242,   118,
     119,   132,   134,   271,   139,   278,    61,   160,   147,   135,
     124,   243,    15,    16,   146,   256,   262,   140,   162,   148,
     150,    10,   151,    54,    55,    58,   161,   154,     7,   200,
     271,   164,    57,   205,   163,   208,   177,   185,   194,   209,
     162,   212,   174,    10,   124,    54,    55,   271,   296,   214,
     271,   215,   219,   184,    57,   222,   296,    59,   226,   227,
     244,   140,    60,   193,   225,   249,   125,   237,   246,    15,
      16,   263,   250,   141,   253,    10,   264,    54,    55,    10,
     229,    54,    55,   265,   231,   279,    57,   165,   281,   257,
      57,   164,   178,   188,   195,    61,   289,    36,   290,   177,
     125,    15,    16,   292,   293,    15,    16,   237,   204,   202,
     185,    10,   232,    54,    55,   272,   233,   141,   230,   268,
     194,   207,    57,    10,   238,    54,    55,   257,   235,   234,
     163,   283,   295,   255,    57,   305,   285,    15,    16,     0,
       0,     0,   272,     0,     0,     0,   258,   165,     0,    15,
      16,     0,   163,     0,     0,   178,     0,     0,     0,   272,
     297,     0,   272,     0,   238,     0,   188,     0,   297,     0,
       7,     0,   273,     0,     0,     0,   195,     0,     0,     0,
     239,     0,     8,     0,   258,     0,    10,   164,    54,    55,
       0,    56,     0,     0,     0,     0,     0,    57,    58,   273,
      59,     0,   259,     0,     0,    60,    61,    62,    63,   164,
       0,     7,    15,    16,     0,     0,   273,   298,     0,   273,
     239,     0,     0,     8,     0,   298,     0,    10,   274,    54,
      55,    10,   110,    54,    55,     0,   267,     0,    57,    58,
     259,    59,    57,   165,     0,     0,    60,    61,    62,    63,
       7,     0,     0,    15,    16,   274,     0,    15,    16,     0,
       0,     0,     8,     0,     0,   165,    10,     0,    54,    55,
       0,   206,   274,   299,     0,   274,     0,    57,    58,     7,
      59,   299,     0,     0,     0,    60,    61,    62,    63,     0,
       0,     8,    15,    16,   160,    10,     0,    54,    55,     0,
     245,     0,     0,     0,     0,     0,    57,    58,    10,    59,
      54,    55,   160,   223,    60,    61,    62,    63,     0,    57,
       0,    15,    16,     0,   160,     0,    10,     0,    54,    55,
       0,   270,     0,     0,   160,     0,     0,    57,    10,     0,
      54,    55,     0,   284,   160,     0,     0,     0,    10,    57,
      54,    55,     0,   287,   160,     0,     0,     0,    10,    57,
      54,    55,     0,   291,   160,     0,     0,     0,    10,    57,
      54,    55,     0,   294,   160,     0,     0,     0,    10,    57,
      54,    55,     0,   303,     0,     0,     0,     0,    10,    57,
      54,    55,     0,   304,     0,     0,     0,     0,     0,    57
};

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-267)))

#define yytable_value_is_error(Yytable_value) \
  YYID (0)

static const yytype_int16 yycheck[] =
{
       5,     5,    66,     5,   167,   152,   112,    37,   248,   275,
      33,    34,    17,    17,   100,    17,     5,    86,    27,     7,
      86,     9,     3,    17,    93,   172,     7,    93,    17,   295,
       7,    22,     9,    24,    25,   275,    27,    46,    22,     0,
      24,    25,    33,    27,    21,    36,    14,    26,    16,    33,
      41,   120,   292,   293,   120,   295,    21,    48,    49,    89,
       8,   301,    28,    28,    48,    49,   152,    97,   137,   175,
      38,   137,    28,    39,    22,   105,    24,    25,   108,   102,
      20,    86,   229,    39,    86,    33,    26,    22,    93,    24,
      25,    93,    27,    14,     7,    16,    21,    86,    33,    38,
     104,    36,   107,    28,    93,    26,    41,   112,   113,   114,
     112,   113,   114,    48,    49,   120,    28,    14,   120,    16,
      34,    35,   285,   112,   113,   114,     6,    39,   158,    26,
      28,   120,   137,     6,     6,   137,     3,     7,    18,    19,
       7,    39,    22,   207,    26,    18,    19,    27,   137,    22,
      22,     5,    24,    25,    27,    27,    38,    37,    14,    21,
       3,    33,   167,    17,    37,    45,    28,    47,    48,    49,
     175,    29,    45,   175,    47,    48,    49,    22,    38,    24,
      25,   186,    27,     6,   186,     3,   175,    38,    33,     7,
      20,   196,    29,    86,   196,   200,    26,   186,   200,    22,
      93,    24,    25,    48,    49,     3,    20,   196,    20,     7,
      33,   200,    26,    20,   107,    26,    20,   222,   222,   112,
     113,   114,   252,    38,   247,     7,    22,   120,    24,    25,
      20,    27,    26,    26,    26,   240,    35,    33,   240,    14,
      14,     7,    14,   248,   137,   249,    42,     8,    26,    14,
      86,   240,    48,    49,    14,   260,   260,    93,   263,    26,
       3,    22,     3,    24,    25,    34,    27,     3,     6,    26,
     275,   107,    33,     5,   167,    21,   112,   113,   114,    21,
     285,    21,   175,    22,   120,    24,    25,   292,   293,    14,
     295,    39,    46,   186,    33,    26,   301,    36,    21,    21,
      27,   137,    41,   196,    39,    16,    86,   200,    27,    48,
      49,    26,    14,    93,    38,    22,    14,    24,    25,    22,
      27,    24,    25,    14,    27,    14,    33,   107,     7,   222,
      33,   167,   112,   113,   114,    42,    14,    17,    39,   175,
     120,    48,    49,    26,    26,    48,    49,   240,   137,   120,
     186,    22,   186,    24,    25,   248,    27,   137,   175,   240,
     196,   148,    33,    22,   200,    24,    25,   260,    27,   196,
     263,   260,   292,   220,    33,   301,   263,    48,    49,    -1,
      -1,    -1,   275,    -1,    -1,    -1,   222,   167,    -1,    48,
      49,    -1,   285,    -1,    -1,   175,    -1,    -1,    -1,   292,
     293,    -1,   295,    -1,   240,    -1,   186,    -1,   301,    -1,
       6,    -1,   248,    -1,    -1,    -1,   196,    -1,    -1,    -1,
     200,    -1,    18,    -1,   260,    -1,    22,   263,    24,    25,
      -1,    27,    -1,    -1,    -1,    -1,    -1,    33,    34,   275,
      36,    -1,   222,    -1,    -1,    41,    42,    43,    44,   285,
      -1,     6,    48,    49,    -1,    -1,   292,   293,    -1,   295,
     240,    -1,    -1,    18,    -1,   301,    -1,    22,   248,    24,
      25,    22,    27,    24,    25,    -1,    27,    -1,    33,    34,
     260,    36,    33,   263,    -1,    -1,    41,    42,    43,    44,
       6,    -1,    -1,    48,    49,   275,    -1,    48,    49,    -1,
      -1,    -1,    18,    -1,    -1,   285,    22,    -1,    24,    25,
      -1,    27,   292,   293,    -1,   295,    -1,    33,    34,     6,
      36,   301,    -1,    -1,    -1,    41,    42,    43,    44,    -1,
      -1,    18,    48,    49,     8,    22,    -1,    24,    25,    -1,
      27,    -1,    -1,    -1,    -1,    -1,    33,    34,    22,    36,
      24,    25,     8,    27,    41,    42,    43,    44,    -1,    33,
      -1,    48,    49,    -1,     8,    -1,    22,    -1,    24,    25,
      -1,    27,    -1,    -1,     8,    -1,    -1,    33,    22,    -1,
      24,    25,    -1,    27,     8,    -1,    -1,    -1,    22,    33,
      24,    25,    -1,    27,     8,    -1,    -1,    -1,    22,    33,
      24,    25,    -1,    27,     8,    -1,    -1,    -1,    22,    33,
      24,    25,    -1,    27,     8,    -1,    -1,    -1,    22,    33,
      24,    25,    -1,    27,    -1,    -1,    -1,    -1,    22,    33,
      24,    25,    -1,    27,    -1,    -1,    -1,    -1,    -1,    33
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    17,    51,    52,     0,    26,    53,     6,    18,    19,
      22,    27,    37,    45,    47,    48,    49,    54,    55,    56,
      57,    61,    65,    66,    98,   109,   110,   111,   112,     7,
      38,     7,     3,    38,    38,    27,    55,    20,    26,    14,
      16,    26,     7,     9,    69,    70,    14,    16,    26,    14,
      69,    69,    70,    71,    24,    25,    27,    33,    34,    36,
      41,    42,    43,    44,    57,    61,    67,    68,    73,    74,
      77,    78,    81,    82,    84,    85,    92,    98,    99,   100,
     105,   108,   109,   110,     3,     7,    58,    29,    29,    28,
      39,     3,     7,    62,    39,    39,    21,    28,    20,    20,
      26,    20,    38,    14,    16,    38,     7,    26,    38,    20,
      27,    68,    26,    26,    26,    35,    88,    89,    14,    14,
      59,    60,    61,    74,    78,    81,    92,    99,   109,   110,
       3,     7,     7,    70,    14,    14,    61,    63,    64,    74,
      78,    81,    92,    99,   109,   110,    14,    26,    26,    70,
       3,     3,    83,    85,     3,    69,    98,    70,    20,    26,
       8,    27,    61,    74,    78,    81,    95,   106,   107,    70,
       3,     7,    27,    61,    74,    75,    76,    78,    81,   100,
     109,   110,    27,    61,    74,    78,    79,    80,    81,   109,
     110,    27,    61,    74,    78,    81,    86,    87,   109,   110,
      26,    27,    60,    27,    64,     5,    27,    67,    21,    21,
      85,    89,    21,    39,    14,    39,    21,    70,    72,    46,
     101,   102,    26,    27,   107,    39,    21,    21,    89,    27,
      76,    27,    80,    27,    87,    27,    61,    74,    78,    81,
      90,    91,   109,   110,    27,    27,    27,    20,    26,    16,
      14,    21,    28,    38,    27,   102,    61,    74,    78,    81,
      96,    97,    98,    26,    14,    14,    89,    27,    91,    69,
      27,    61,    74,    78,    81,    93,    94,    95,    98,    14,
      70,     7,    27,    97,    27,   106,    21,    27,    94,    14,
      39,    27,    26,    26,    27,    93,    61,    74,    78,    81,
      95,   103,   104,    27,    27,   104
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))

/* Error token number */
#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (YYID (N))                                                     \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (YYID (0))
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef __attribute__
/* This feature is available in gcc versions 2.5 and later.  */
# if (! defined __GNUC__ || __GNUC__ < 2 \
      || (__GNUC__ == 2 && __GNUC_MINOR__ < 5))
#  define __attribute__(Spec) /* empty */
# endif
#endif

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

__attribute__((__unused__))
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
#else
static unsigned
yy_location_print_ (yyo, yylocp)
    FILE *yyo;
    YYLTYPE const * const yylocp;
#endif
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += fprintf (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += fprintf (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += fprintf (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += fprintf (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += fprintf (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */
#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value, Location); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    YYLTYPE const * const yylocationp;
#endif
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
  YYUSE (yylocationp);
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
        break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep, yylocationp)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    YYLTYPE const * const yylocationp;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yylsp, yyrule)
    YYSTYPE *yyvsp;
    YYLTYPE *yylsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       , &(yylsp[(yyi + 1) - (yynrhs)])		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, yylsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULL, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULL;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULL, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
#else
static void
yydestruct (yymsg, yytype, yyvaluep, yylocationp)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
#endif
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}




/* The lookahead symbol.  */
int yychar;


#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval YY_INITIAL_VALUE(yyval_default);

/* Location data for the lookahead symbol.  */
YYLTYPE yylloc
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;


/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.
       `yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[3];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  yylsp[0] = yylloc;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;
	YYLTYPE *yyls1 = yyls;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);

	yyls = yyls1;
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
	YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
/* Line 1792 of yacc.c  */
#line 174 "parser.y"
    {cout << "Nothing to parse\n";}
    break;

  case 3:
/* Line 1792 of yacc.c  */
#line 175 "parser.y"
    {
   root_ptr = new Block(nullptr); 
   current_block_ptr = root_ptr;
   scopes_stack.push(current_block_ptr);
  }
    break;

  case 4:
/* Line 1792 of yacc.c  */
#line 180 "parser.y"
    {

  assert((yyvsp[(3) - (3)].child) != nullptr);
 
cout << (yyvsp[(3) - (3)].child)->render();
 delete root_ptr;
}
    break;

  case 5:
/* Line 1792 of yacc.c  */
#line 189 "parser.y"
    {(yyval.child) = new StylesheetDeclaration(nullptr);}
    break;

  case 6:
/* Line 1792 of yacc.c  */
#line 192 "parser.y"
    {
    if(!(root_ptr->all_xml_nodes_ok())){
      std::cout << "no closed tags found\n";
      throw;
    }
    (yyval.child) = new StylesheetDeclaration((yyvsp[(2) - (3)].block));
}
    break;

  case 7:
/* Line 1792 of yacc.c  */
#line 202 "parser.y"
    {
  (yyval.block) = root_ptr;
  (yyval.block)->add_child((yyvsp[(1) - (1)].child));
}
    break;

  case 8:
/* Line 1792 of yacc.c  */
#line 206 "parser.y"
    {(yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 9:
/* Line 1792 of yacc.c  */
#line 210 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 10:
/* Line 1792 of yacc.c  */
#line 211 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 11:
/* Line 1792 of yacc.c  */
#line 212 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 12:
/* Line 1792 of yacc.c  */
#line 213 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 13:
/* Line 1792 of yacc.c  */
#line 214 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 14:
/* Line 1792 of yacc.c  */
#line 215 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 15:
/* Line 1792 of yacc.c  */
#line 216 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 16:
/* Line 1792 of yacc.c  */
#line 217 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child); current_block_ptr->push_on_stack(static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name());}
    break;

  case 17:
/* Line 1792 of yacc.c  */
#line 218 "parser.y"
    {
 if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
 }
 (yyval.child) = (yyvsp[(1) - (1)].child);
}
    break;

  case 18:
/* Line 1792 of yacc.c  */
#line 233 "parser.y"
    {(yyval.child) = new ImportDeclaration(*(yyvsp[(2) - (3)].str));}
    break;

  case 19:
/* Line 1792 of yacc.c  */
#line 238 "parser.y"
    {

  if(!current_block_ptr->search_current_scope((yyvsp[(2) - (5)].sym)->get_sym_name(), T_VAR) && current_block_ptr->search((yyvsp[(4) - (5)].sym)->get_sym_name(), T_VAR)){
      current_block_ptr->add_symbol((yyvsp[(2) - (5)].sym)->get_sym_name(), T_VAR);
      (yyval.child) = new ParamBlock((yyvsp[(2) - (5)].sym)->get_sym_name(), "$"+(yyvsp[(4) - (5)].sym)->get_sym_name());
    }
    else{
      std::cout << (yyvsp[(4) - (5)].sym)->get_sym_name() << " : Symbol not found.\n";
      throw;
    }
}
    break;

  case 20:
/* Line 1792 of yacc.c  */
#line 251 "parser.y"
    {
  if(!current_block_ptr->search_current_scope((yyvsp[(2) - (5)].sym)->get_sym_name(), T_VAR)){
   current_block_ptr->add_symbol((yyvsp[(2) - (5)].sym)->get_sym_name(), T_VAR);
   (yyval.child) = new ParamBlock((yyvsp[(2) - (5)].sym)->get_sym_name(), *(yyvsp[(4) - (5)].str));
  }
  else{
   std::cout << (yyvsp[(2) - (5)].sym)->get_sym_name() << " : Symbol redeclared.\n";
   throw;
  }
}
    break;

  case 21:
/* Line 1792 of yacc.c  */
#line 262 "parser.y"
    {
  if(!current_block_ptr->search_current_scope((yyvsp[(2) - (3)].sym)->get_sym_name(), T_VAR)){
   current_block_ptr->add_symbol((yyvsp[(2) - (3)].sym)->get_sym_name(), T_VAR);
   (yyval.child) = new ParamBlock((yyvsp[(2) - (3)].sym)->get_sym_name(), "''");
  }
  else{
   std::cout << (yyvsp[(2) - (3)].sym)->get_sym_name() << " : Symbol redeclared.\n";
   throw;
  }
}
    break;

  case 22:
/* Line 1792 of yacc.c  */
#line 273 "parser.y"
    {
  Block *b = current_block_ptr;
  current_block_ptr = new Block(nullptr);
  current_block_ptr->add_parent(b);
  scopes_stack.push(current_block_ptr);
 }
    break;

  case 23:
/* Line 1792 of yacc.c  */
#line 280 "parser.y"
    {
  //Process and remove the top element from stack
  //current_block_ptr = scopes_stack.top();
  //scopes_stack.pop();

  if(!current_block_ptr->search_current_scope((yyvsp[(2) - (6)].sym)->get_sym_name(), T_VAR)){
    current_block_ptr->add_symbol((yyvsp[(2) - (6)].sym)->get_sym_name(), T_VAR);
   (yyval.child) = new ParamBlock((yyvsp[(2) - (6)].sym)->get_sym_name(), (yyvsp[(5) - (6)].block));
   
   scopes_stack.pop();
   current_block_ptr = scopes_stack.top();
  }
  else{
   std::cout << (yyvsp[(2) - (6)].sym)->get_sym_name() << " : Symbol redeclared.\n";
   throw;
  }
}
    break;

  case 24:
/* Line 1792 of yacc.c  */
#line 301 "parser.y"
    {
  //scopes_stack.push(current_block_ptr); //save the current scope
  //$$ = new Block($1);
  //$$->add_parent(current_block_ptr);
  //current_block_ptr = $$;
  (yyval.block) = current_block_ptr->add_child((yyvsp[(1) - (1)].child));
}
    break;

  case 25:
/* Line 1792 of yacc.c  */
#line 308 "parser.y"
    {(yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 26:
/* Line 1792 of yacc.c  */
#line 312 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 27:
/* Line 1792 of yacc.c  */
#line 313 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 28:
/* Line 1792 of yacc.c  */
#line 314 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 29:
/* Line 1792 of yacc.c  */
#line 315 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 30:
/* Line 1792 of yacc.c  */
#line 316 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 31:
/* Line 1792 of yacc.c  */
#line 317 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 32:
/* Line 1792 of yacc.c  */
#line 318 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);  current_block_ptr->push_on_stack(static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name());}
    break;

  case 33:
/* Line 1792 of yacc.c  */
#line 319 "parser.y"
    {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    (yyval.child) = (yyvsp[(1) - (1)].child);
  }
    break;

  case 34:
/* Line 1792 of yacc.c  */
#line 337 "parser.y"
    {

  if(!current_block_ptr->search_current_scope((yyvsp[(2) - (5)].sym)->get_sym_name(), T_VAR) /*&& current_block_ptr->search($4->get_sym_name(), T_VAR)*/){
      current_block_ptr->add_symbol((yyvsp[(2) - (5)].sym)->get_sym_name(), T_VAR);
      (yyval.child) = new VariableBlock((yyvsp[(2) - (5)].sym)->get_sym_name(), "$"+(yyvsp[(4) - (5)].sym)->get_sym_name(), current_block_ptr);
    }
    else{
      std::cout << (yyvsp[(4) - (5)].sym)->get_sym_name() << " : Symbol not found.\n";
      throw;
    }
}
    break;

  case 35:
/* Line 1792 of yacc.c  */
#line 350 "parser.y"
    {
  if(!current_block_ptr->search_current_scope((yyvsp[(2) - (5)].sym)->get_sym_name(), T_VAR)){
   current_block_ptr->add_symbol((yyvsp[(2) - (5)].sym)->get_sym_name(), T_VAR);
   (yyval.child) = new VariableBlock((yyvsp[(2) - (5)].sym)->get_sym_name(), *(yyvsp[(4) - (5)].str));
  }
  else{
   std::cout << (yyvsp[(2) - (5)].sym)->get_sym_name() << " : Symbol redeclared.\n";
   throw;
  }
}
    break;

  case 36:
/* Line 1792 of yacc.c  */
#line 361 "parser.y"
    {
  if(!current_block_ptr->search_current_scope((yyvsp[(2) - (3)].sym)->get_sym_name(), T_VAR)){
   current_block_ptr->add_symbol((yyvsp[(2) - (3)].sym)->get_sym_name(), T_VAR);
   (yyval.child) = new VariableBlock((yyvsp[(2) - (3)].sym)->get_sym_name(), "''");
  }
  else{
   std::cout << (yyvsp[(2) - (3)].sym)->get_sym_name() << " : Symbol redeclared.\n";
   throw;
  }
}
    break;

  case 37:
/* Line 1792 of yacc.c  */
#line 372 "parser.y"
    {
  Block *b = current_block_ptr;
  current_block_ptr = new Block(nullptr);
  current_block_ptr->add_parent(b);
  scopes_stack.push(current_block_ptr);
  }
    break;

  case 38:
/* Line 1792 of yacc.c  */
#line 378 "parser.y"
    {
  //Process and remove the top element from stack
  //current_block_ptr = scopes_stack.top();
  //scopes_stack.pop();

  if(!current_block_ptr->search_current_scope((yyvsp[(2) - (6)].sym)->get_sym_name(), T_VAR)){
    current_block_ptr->add_symbol((yyvsp[(2) - (6)].sym)->get_sym_name(), T_VAR);
   (yyval.child) = new VariableBlock((yyvsp[(2) - (6)].sym)->get_sym_name(), (yyvsp[(5) - (6)].block));
   scopes_stack.pop();
   current_block_ptr = scopes_stack.top();
  }
  else{
   std::cout << (yyvsp[(2) - (6)].sym)->get_sym_name() << " : Symbol redeclared.\n";
   throw;
  }
}
    break;

  case 39:
/* Line 1792 of yacc.c  */
#line 398 "parser.y"
    {
  //scopes_stack.push(current_block_ptr); //save the current scope
  // $$ = new Block($1);
  //$$->add_parent(current_block_ptr);
  //current_block_ptr = $$;
  (yyval.block) = current_block_ptr->add_child((yyvsp[(1) - (1)].child));
}
    break;

  case 40:
/* Line 1792 of yacc.c  */
#line 405 "parser.y"
    {(yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 41:
/* Line 1792 of yacc.c  */
#line 409 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 42:
/* Line 1792 of yacc.c  */
#line 410 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 43:
/* Line 1792 of yacc.c  */
#line 411 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 44:
/* Line 1792 of yacc.c  */
#line 412 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 45:
/* Line 1792 of yacc.c  */
#line 413 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 46:
/* Line 1792 of yacc.c  */
#line 414 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 47:
/* Line 1792 of yacc.c  */
#line 415 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);  current_block_ptr->push_on_stack(static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name());}
    break;

  case 48:
/* Line 1792 of yacc.c  */
#line 416 "parser.y"
    {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    (yyval.child) = (yyvsp[(1) - (1)].child);
  }
    break;

  case 49:
/* Line 1792 of yacc.c  */
#line 433 "parser.y"
    {
  for(auto b = (yyvsp[(3) - (4)].prop_list_decl)->begin(); b != (yyvsp[(3) - (4)].prop_list_decl)->end(); ++b){
                  if(b->first == "name"){
                                    try{
                     root_ptr->add_symbol(b->second, T_TEMPLATE);
                                    }catch(SymbolExistsException e){
                                        std::cout << b->second << ':' << e.what() << '\n';
                        throw e;
                                    }
                  }
                }
  (yyval.prop_list_decl) = (yyvsp[(3) - (4)].prop_list_decl);
  //Block* ptr = current_block_ptr;
  current_block_ptr = new Block(nullptr);
  current_block_ptr->add_parent(root_ptr);
  scopes_stack.push(current_block_ptr);
}
    break;

  case 50:
/* Line 1792 of yacc.c  */
#line 453 "parser.y"
    {
    try{
      (yyval.child) = new TemplateDeclaration((yyvsp[(1) - (3)].prop_list_decl), nullptr, nullptr);
    }catch(InvalidPropertyException e){
      std::cout << e.what();
      throw;
    }
}
    break;

  case 51:
/* Line 1792 of yacc.c  */
#line 464 "parser.y"
    {
  try{
    if(!((yyvsp[(3) - (4)].block)->all_xml_nodes_ok())){
      std::cout << "no closed tags found\n";
      throw;
    }
    (yyval.child) = new TemplateDeclaration((yyvsp[(1) - (4)].prop_list_decl), nullptr, (yyvsp[(3) - (4)].block));
    //pop the current scope as we are done with it
    scopes_stack.pop();
    current_block_ptr = scopes_stack.top();
  }catch(InvalidPropertyException e){
    std::cout << e.what();
    throw;
  }
}
    break;

  case 52:
/* Line 1792 of yacc.c  */
#line 484 "parser.y"
    {
  try{
    (yyval.child) = new TemplateDeclaration((yyvsp[(1) - (6)].prop_list_decl), (yyvsp[(3) - (6)].arg_list_decl), nullptr);
  }catch(InvalidPropertyException e){
    std::cout << e.what();
    throw;
  }
}
    break;

  case 53:
/* Line 1792 of yacc.c  */
#line 497 "parser.y"
    {
  try{
    (yyval.child) = new TemplateDeclaration((yyvsp[(1) - (7)].prop_list_decl), (yyvsp[(3) - (7)].arg_list_decl), (yyvsp[(6) - (7)].block));
  }catch(InvalidPropertyException e){
    std::cout << e.what();
    throw;
  }
}
    break;

  case 54:
/* Line 1792 of yacc.c  */
#line 509 "parser.y"
    {
  (yyval.block) = current_block_ptr->add_child((yyvsp[(1) - (1)].child));//current_block_ptr;//new Block($1);
  //$$->add_parent(current_block_ptr);
  //current_block_ptr = $$;
  if(ParamBlock *p = dynamic_cast<ParamBlock*>((yyvsp[(1) - (1)].child)))
    last_template_child_type = 'p';
  else
    last_template_child_type = ' ';
}
    break;

  case 55:
/* Line 1792 of yacc.c  */
#line 520 "parser.y"
    {
  if(ParamBlock *p = dynamic_cast<ParamBlock*>((yyvsp[(2) - (2)].child))){
    if(!(last_template_child_type == 'p')){
     std::cout << "A param (if any) should be the first element in a template definition\n";
     throw;
    }
    else{
      (yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));
    }
  }
  else{
    (yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));
    last_template_child_type = ' ';
  }
  /*  if($1->get_children_count() >= 0 && (p = dynamic_cast<ParamBlock*>($2))){
    std::cout << "A param (if any) should be the first element in a template definition\n";
    throw;
  }
  $$ = $1->add_child($2);*/
}
    break;

  case 56:
/* Line 1792 of yacc.c  */
#line 543 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 57:
/* Line 1792 of yacc.c  */
#line 544 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 58:
/* Line 1792 of yacc.c  */
#line 545 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 59:
/* Line 1792 of yacc.c  */
#line 546 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 60:
/* Line 1792 of yacc.c  */
#line 547 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 61:
/* Line 1792 of yacc.c  */
#line 548 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 62:
/* Line 1792 of yacc.c  */
#line 549 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 63:
/* Line 1792 of yacc.c  */
#line 550 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 64:
/* Line 1792 of yacc.c  */
#line 551 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 65:
/* Line 1792 of yacc.c  */
#line 552 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 66:
/* Line 1792 of yacc.c  */
#line 553 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 67:
/* Line 1792 of yacc.c  */
#line 554 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 68:
/* Line 1792 of yacc.c  */
#line 555 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);  current_block_ptr->push_on_stack(static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name());}
    break;

  case 69:
/* Line 1792 of yacc.c  */
#line 556 "parser.y"
    {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    (yyval.child) = (yyvsp[(1) - (1)].child);
  }
    break;

  case 70:
/* Line 1792 of yacc.c  */
#line 572 "parser.y"
    {(yyval.prop_list_decl) = new PropertyDeclarationList((yyvsp[(1) - (1)].prop_decl)); }
    break;

  case 71:
/* Line 1792 of yacc.c  */
#line 573 "parser.y"
    {(yyval.prop_list_decl) = (yyvsp[(1) - (3)].prop_list_decl)->add_property((yyvsp[(3) - (3)].prop_decl));}
    break;

  case 72:
/* Line 1792 of yacc.c  */
#line 577 "parser.y"
    {(yyval.prop_decl) = new PropertyDeclaration((yyvsp[(1) - (3)].sym)->get_sym_name(), (yyvsp[(3) - (3)].sym)->get_sym_name());}
    break;

  case 73:
/* Line 1792 of yacc.c  */
#line 578 "parser.y"
    {(yyval.prop_decl) = new PropertyDeclaration((yyvsp[(1) - (3)].sym)->get_sym_name(), (yyvsp[(3) - (3)].sym)->get_sym_name());}
    break;

  case 74:
/* Line 1792 of yacc.c  */
#line 579 "parser.y"
    {(yyval.prop_decl) = new PropertyDeclaration((yyvsp[(1) - (3)].sym)->get_sym_name(), *(yyvsp[(3) - (3)].str));}
    break;

  case 75:
/* Line 1792 of yacc.c  */
#line 582 "parser.y"
    {(yyval.arg_list_decl) = new ArgumentDeclarationList((yyvsp[(1) - (1)].prop_decl));}
    break;

  case 76:
/* Line 1792 of yacc.c  */
#line 583 "parser.y"
    {(yyval.arg_list_decl) = reinterpret_cast<ArgumentDeclarationList*>((yyvsp[(1) - (3)].arg_list_decl)->add_property((yyvsp[(3) - (3)].prop_decl)));}
    break;

  case 77:
/* Line 1792 of yacc.c  */
#line 586 "parser.y"
    {(yyval.with_param_list_decl) = new WithParamDeclarationList((yyvsp[(1) - (1)].prop_decl));}
    break;

  case 78:
/* Line 1792 of yacc.c  */
#line 587 "parser.y"
    {(yyval.with_param_list_decl) = reinterpret_cast<WithParamDeclarationList*>((yyvsp[(1) - (3)].with_param_list_decl)->add_property((yyvsp[(3) - (3)].prop_decl)));}
    break;

  case 79:
/* Line 1792 of yacc.c  */
#line 601 "parser.y"
    {
  (yyval.str) = (yyvsp[(3) - (4)].str);
  //Block* ptr = current_block_ptr;
  Block* block = current_block_ptr; //save parent block ptr
  current_block_ptr = new Block(nullptr); //create a new block for if
  current_block_ptr->add_parent(block); //add parent to the if block
  scopes_stack.push(current_block_ptr);
}
    break;

  case 80:
/* Line 1792 of yacc.c  */
#line 611 "parser.y"
    {(yyval.child) = new IfBlock(*(yyvsp[(1) - (3)].str), nullptr);}
    break;

  case 81:
/* Line 1792 of yacc.c  */
#line 612 "parser.y"
    {
   if(!(current_block_ptr->all_xml_nodes_ok())){
      std::cout << "no closed tags found\n";
      throw;
   }
   (yyval.child) = new IfBlock(*(yyvsp[(1) - (4)].str), (yyvsp[(3) - (4)].block));
   scopes_stack.pop();
   current_block_ptr = scopes_stack.top();
  }
    break;

  case 82:
/* Line 1792 of yacc.c  */
#line 622 "parser.y"
    {
  Block *block = new Block(new WhenBlock(*(yyvsp[(1) - (4)].str), nullptr));
  block->add_child((yyvsp[(4) - (4)].child));
  (yyval.child) = new CaseBlock(block) ;
}
    break;

  case 83:
/* Line 1792 of yacc.c  */
#line 628 "parser.y"
    {
  Block *block = new Block(new WhenBlock(*(yyvsp[(1) - (5)].str), (yyvsp[(3) - (5)].block)));
  block->add_child((yyvsp[(5) - (5)].child));
  (yyval.child) = new CaseBlock(block) ;
  scopes_stack.pop();
  current_block_ptr = scopes_stack.top();
}
    break;

  case 84:
/* Line 1792 of yacc.c  */
#line 638 "parser.y"
    {
  //$$ = new Block($1);
  //current_block_ptr is already pointing to a new Block.
  (yyval.block) = current_block_ptr->add_child((yyvsp[(1) - (1)].child));//current_block_ptr;
  //$$->add_parent(current_block_ptr);

  //current_block_ptr = $$;

}
    break;

  case 85:
/* Line 1792 of yacc.c  */
#line 647 "parser.y"
    {(yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 86:
/* Line 1792 of yacc.c  */
#line 651 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 87:
/* Line 1792 of yacc.c  */
#line 652 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 88:
/* Line 1792 of yacc.c  */
#line 653 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 89:
/* Line 1792 of yacc.c  */
#line 654 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 90:
/* Line 1792 of yacc.c  */
#line 655 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 91:
/* Line 1792 of yacc.c  */
#line 656 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);  current_block_ptr->push_on_stack(static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name());}
    break;

  case 92:
/* Line 1792 of yacc.c  */
#line 657 "parser.y"
    {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    (yyval.child) = (yyvsp[(1) - (1)].child);
  }
    break;

  case 93:
/* Line 1792 of yacc.c  */
#line 680 "parser.y"
    {
 (yyval.str) = (yyvsp[(3) - (4)].str);
  //Block* ptr = current_block_ptr;
  Block* block = current_block_ptr; //save parent block ptr
  current_block_ptr = new Block(nullptr); //create a new block for for
  current_block_ptr->add_parent(block); //add parent to the for block
  scopes_stack.push(current_block_ptr);
}
    break;

  case 94:
/* Line 1792 of yacc.c  */
#line 691 "parser.y"
    {(yyval.child) = new ForeachBlock(*(yyvsp[(1) - (3)].str), nullptr); }
    break;

  case 95:
/* Line 1792 of yacc.c  */
#line 692 "parser.y"
    {
   if(!(current_block_ptr->all_xml_nodes_ok())){
      std::cout << "no closed tags found\n";
      throw;
   }
   (yyval.child) = new ForeachBlock(*(yyvsp[(1) - (4)].str), (yyvsp[(3) - (4)].block)); 
   scopes_stack.pop();
   current_block_ptr = scopes_stack.top();
}
    break;

  case 96:
/* Line 1792 of yacc.c  */
#line 703 "parser.y"
    {
   //current_block_ptr is already pointing to a new Block.
  (yyval.block) = current_block_ptr->add_child((yyvsp[(1) - (1)].child));
}
    break;

  case 97:
/* Line 1792 of yacc.c  */
#line 707 "parser.y"
    {(yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 98:
/* Line 1792 of yacc.c  */
#line 710 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 99:
/* Line 1792 of yacc.c  */
#line 711 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 100:
/* Line 1792 of yacc.c  */
#line 712 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 101:
/* Line 1792 of yacc.c  */
#line 713 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 102:
/* Line 1792 of yacc.c  */
#line 714 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);  current_block_ptr->push_on_stack(static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name());}
    break;

  case 103:
/* Line 1792 of yacc.c  */
#line 715 "parser.y"
    {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    (yyval.child) = (yyvsp[(1) - (1)].child);
  }
    break;

  case 104:
/* Line 1792 of yacc.c  */
#line 733 "parser.y"
    {
   Block *b = nullptr;
   if((yyvsp[(3) - (5)].child) != nullptr){
     b = static_cast<Block*>((yyvsp[(3) - (5)].child))->add_child((yyvsp[(4) - (5)].child));
   }
   (yyval.child) = new CaseBlock(b);
 }
    break;

  case 105:
/* Line 1792 of yacc.c  */
#line 745 "parser.y"
    {

  Block *b = new Block((yyvsp[(1) - (2)].child));
  b->add_child((yyvsp[(2) - (2)].child));

  (yyval.child) = new CaseBlock(b);
}
    break;

  case 106:
/* Line 1792 of yacc.c  */
#line 754 "parser.y"
    {(yyval.child) = new Block((yyvsp[(1) - (1)].child));}
    break;

  case 107:
/* Line 1792 of yacc.c  */
#line 755 "parser.y"
    {(yyval.child) = static_cast<Block*>((yyvsp[(1) - (2)].child))->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 108:
/* Line 1792 of yacc.c  */
#line 759 "parser.y"
    {
 (yyval.str) = (yyvsp[(3) - (4)].str);
  //Block* ptr = current_block_ptr;
  Block* block = current_block_ptr; //save parent block ptr
  current_block_ptr = new Block(nullptr); //create a new block for for
  current_block_ptr->add_parent(block); //add parent to the for block
  scopes_stack.push(current_block_ptr);
}
    break;

  case 109:
/* Line 1792 of yacc.c  */
#line 768 "parser.y"
    {(yyval.child) = new WhenBlock(*(yyvsp[(1) - (3)].str), nullptr);}
    break;

  case 110:
/* Line 1792 of yacc.c  */
#line 771 "parser.y"
    {
  if(!(current_block_ptr->all_xml_nodes_ok())){
      std::cout << "no closed tags found\n";
      throw;
   }
  (yyval.child) = new WhenBlock(*(yyvsp[(1) - (4)].str), static_cast<Block*>((yyvsp[(3) - (4)].block)));
  scopes_stack.pop();
  current_block_ptr = scopes_stack.top();
}
    break;

  case 111:
/* Line 1792 of yacc.c  */
#line 783 "parser.y"
    {  
  //current_block_ptr is already pointing to a new Block.
  (yyval.block) = current_block_ptr->add_child((yyvsp[(1) - (1)].child));
}
    break;

  case 112:
/* Line 1792 of yacc.c  */
#line 787 "parser.y"
    {(yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 113:
/* Line 1792 of yacc.c  */
#line 790 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 114:
/* Line 1792 of yacc.c  */
#line 791 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 115:
/* Line 1792 of yacc.c  */
#line 792 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 116:
/* Line 1792 of yacc.c  */
#line 793 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 117:
/* Line 1792 of yacc.c  */
#line 794 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);  current_block_ptr->push_on_stack(static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name());}
    break;

  case 118:
/* Line 1792 of yacc.c  */
#line 795 "parser.y"
    {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    (yyval.child) = (yyvsp[(1) - (1)].child);
  }
    break;

  case 119:
/* Line 1792 of yacc.c  */
#line 812 "parser.y"
    {
  //Block* ptr = current_block_ptr;
  Block* block = current_block_ptr; //save parent block ptr
  current_block_ptr = new Block(nullptr); //create a new block for for
  current_block_ptr->add_parent(block); //add parent to the for block
  scopes_stack.push(current_block_ptr);
}
    break;

  case 120:
/* Line 1792 of yacc.c  */
#line 820 "parser.y"
    {(yyval.child) = nullptr;}
    break;

  case 121:
/* Line 1792 of yacc.c  */
#line 822 "parser.y"
    {(yyval.child) = new OtherwiseBlock(nullptr);}
    break;

  case 122:
/* Line 1792 of yacc.c  */
#line 825 "parser.y"
    {
  if(!(current_block_ptr->all_xml_nodes_ok())){
      std::cout << "no closed tags found\n";
      throw;
   }
  (yyval.child) = new OtherwiseBlock(static_cast<Block*>((yyvsp[(3) - (4)].block)));
  scopes_stack.pop();
  current_block_ptr = scopes_stack.top();
}
    break;

  case 123:
/* Line 1792 of yacc.c  */
#line 836 "parser.y"
    {  
  //current_block_ptr is already pointing to a new Block.
  (yyval.block) = current_block_ptr->add_child((yyvsp[(1) - (1)].child));
}
    break;

  case 124:
/* Line 1792 of yacc.c  */
#line 840 "parser.y"
    {(yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 125:
/* Line 1792 of yacc.c  */
#line 843 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 126:
/* Line 1792 of yacc.c  */
#line 844 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 127:
/* Line 1792 of yacc.c  */
#line 845 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 128:
/* Line 1792 of yacc.c  */
#line 846 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 129:
/* Line 1792 of yacc.c  */
#line 847 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);  current_block_ptr->push_on_stack(static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name());}
    break;

  case 130:
/* Line 1792 of yacc.c  */
#line 848 "parser.y"
    {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>((yyvsp[(1) - (1)].child))->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    (yyval.child) = (yyvsp[(1) - (1)].child);
  }
    break;

  case 131:
/* Line 1792 of yacc.c  */
#line 865 "parser.y"
    {(yyval.child) = new ElementBlock((yyvsp[(3) - (9)].prop_list_decl), (yyvsp[(6) - (9)].prop_list_decl), nullptr);}
    break;

  case 132:
/* Line 1792 of yacc.c  */
#line 867 "parser.y"
    {
  (yyval.child) = new ElementBlock((yyvsp[(3) - (10)].prop_list_decl), (yyvsp[(6) - (10)].prop_list_decl), (yyvsp[(9) - (10)].block));
}
    break;

  case 133:
/* Line 1792 of yacc.c  */
#line 871 "parser.y"
    {
  (yyval.child) = new ElementBlock((yyvsp[(3) - (7)].prop_list_decl), nullptr, (yyvsp[(6) - (7)].block));
}
    break;

  case 134:
/* Line 1792 of yacc.c  */
#line 875 "parser.y"
    {
  (yyval.child) = new ElementBlock((yyvsp[(3) - (6)].prop_list_decl), nullptr, nullptr);
}
    break;

  case 135:
/* Line 1792 of yacc.c  */
#line 882 "parser.y"
    {
  (yyval.block) = new Block((yyvsp[(1) - (1)].child));
  (yyval.block)->add_parent(current_block_ptr);
  current_block_ptr = (yyval.block);
}
    break;

  case 136:
/* Line 1792 of yacc.c  */
#line 887 "parser.y"
    {(yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 137:
/* Line 1792 of yacc.c  */
#line 891 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 138:
/* Line 1792 of yacc.c  */
#line 892 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 139:
/* Line 1792 of yacc.c  */
#line 893 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 140:
/* Line 1792 of yacc.c  */
#line 894 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 141:
/* Line 1792 of yacc.c  */
#line 895 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 142:
/* Line 1792 of yacc.c  */
#line 898 "parser.y"
    {(yyval.child) = new Attribute((yyvsp[(1) - (4)].sym)->get_sym_name(), (yyvsp[(3) - (4)].block));}
    break;

  case 143:
/* Line 1792 of yacc.c  */
#line 903 "parser.y"
    {
  (yyval.block) = new Block((yyvsp[(1) - (1)].child));
  (yyval.block)->add_parent(current_block_ptr);
  current_block_ptr = (yyval.block);
}
    break;

  case 144:
/* Line 1792 of yacc.c  */
#line 908 "parser.y"
    {(yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 145:
/* Line 1792 of yacc.c  */
#line 912 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 146:
/* Line 1792 of yacc.c  */
#line 913 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 147:
/* Line 1792 of yacc.c  */
#line 914 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 148:
/* Line 1792 of yacc.c  */
#line 915 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 149:
/* Line 1792 of yacc.c  */
#line 916 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 150:
/* Line 1792 of yacc.c  */
#line 919 "parser.y"
    {(yyval.child) = new StrayText(*(yyvsp[(1) - (1)].str));}
    break;

  case 151:
/* Line 1792 of yacc.c  */
#line 922 "parser.y"
    {(yyval.child) = new Text(false, (yyvsp[(3) - (4)].child)->render());}
    break;

  case 152:
/* Line 1792 of yacc.c  */
#line 923 "parser.y"
    {(yyval.child) = new Text(false, "");}
    break;

  case 153:
/* Line 1792 of yacc.c  */
#line 925 "parser.y"
    {
  if((yyvsp[(3) - (7)].prop_decl)->get_key() == "disable-output-escaping" && ((yyvsp[(3) - (7)].prop_decl)->get_value() == "yes" || (yyvsp[(3) - (7)].prop_decl)->get_value() == "no")){
   (yyval.child) = new Text(((yyvsp[(3) - (7)].prop_decl)->get_value() == "yes" ? true : false), (yyvsp[(6) - (7)].child)->render());
 }
 else{
   throw InvalidPropertyException();
 }
}
    break;

  case 154:
/* Line 1792 of yacc.c  */
#line 937 "parser.y"
    {
  (yyval.child) = new TemplateCall((yyvsp[(2) - (6)].sym)->get_sym_name(), (yyvsp[(4) - (6)].with_param_list_decl), root_ptr, nullptr);
}
    break;

  case 155:
/* Line 1792 of yacc.c  */
#line 941 "parser.y"
    {
  (yyval.child) = new TemplateCall((yyvsp[(2) - (5)].sym)->get_sym_name(), nullptr, root_ptr, nullptr);
}
    break;

  case 156:
/* Line 1792 of yacc.c  */
#line 945 "parser.y"
    {
  (yyval.child) = new TemplateCall((yyvsp[(2) - (5)].sym)->get_sym_name(), nullptr, root_ptr, (yyvsp[(4) - (5)].block));
}
    break;

  case 157:
/* Line 1792 of yacc.c  */
#line 951 "parser.y"
    {
  (yyval.block) = new Block((yyvsp[(1) - (1)].child));
  (yyval.block)->add_parent(current_block_ptr);
  current_block_ptr = (yyval.block);
  //scopes_stack.push($$); //push the current block (scope) on the stack
}
    break;

  case 158:
/* Line 1792 of yacc.c  */
#line 957 "parser.y"
    {(yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 159:
/* Line 1792 of yacc.c  */
#line 962 "parser.y"
    {
  //current_block_ptr should point to the arg_decls block when validating the arg-name (SYM)
  //scopes_stack.pop();
  //current_block_ptr = scopes_stack.top();
  if(!current_block_ptr->search((yyvsp[(3) - (7)].sym)->get_sym_name(), T_PARAM)){
    (yyval.child) = new WithParamBlock((yyvsp[(3) - (7)].sym)->get_sym_name(), (yyvsp[(6) - (7)].block));
  }
  else{
    std::cout << (yyvsp[(3) - (7)].sym)->get_sym_name() << " : Symbol not found.\n";
    throw;
  }
}
    break;

  case 160:
/* Line 1792 of yacc.c  */
#line 977 "parser.y"
    {
  //  scopes_stack.push(current_block_ptr); //save the current scope for later validations
  (yyval.block) = new Block((yyvsp[(1) - (1)].child));
  (yyval.block)->add_parent(current_block_ptr);
  current_block_ptr = (yyval.block);
}
    break;

  case 161:
/* Line 1792 of yacc.c  */
#line 983 "parser.y"
    {(yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 162:
/* Line 1792 of yacc.c  */
#line 987 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 163:
/* Line 1792 of yacc.c  */
#line 988 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 164:
/* Line 1792 of yacc.c  */
#line 989 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 165:
/* Line 1792 of yacc.c  */
#line 990 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 166:
/* Line 1792 of yacc.c  */
#line 991 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 167:
/* Line 1792 of yacc.c  */
#line 995 "parser.y"
    {(yyval.child) = new Copy((yyvsp[(3) - (6)].prop_decl), nullptr);}
    break;

  case 168:
/* Line 1792 of yacc.c  */
#line 996 "parser.y"
    {(yyval.child) = new Copy((yyvsp[(3) - (7)].prop_decl), (yyvsp[(6) - (7)].block));}
    break;

  case 169:
/* Line 1792 of yacc.c  */
#line 997 "parser.y"
    {(yyval.child) = new Copy(nullptr, nullptr);}
    break;

  case 170:
/* Line 1792 of yacc.c  */
#line 998 "parser.y"
    {(yyval.child) = new Copy(nullptr, (yyvsp[(3) - (4)].block));}
    break;

  case 171:
/* Line 1792 of yacc.c  */
#line 1002 "parser.y"
    {
    (yyval.block) = new Block((yyvsp[(1) - (1)].child));
  (yyval.block)->add_parent(current_block_ptr);
  current_block_ptr = (yyval.block);
}
    break;

  case 172:
/* Line 1792 of yacc.c  */
#line 1007 "parser.y"
    {(yyval.block) = (yyvsp[(1) - (2)].block)->add_child((yyvsp[(2) - (2)].child));}
    break;

  case 173:
/* Line 1792 of yacc.c  */
#line 1011 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 174:
/* Line 1792 of yacc.c  */
#line 1012 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 175:
/* Line 1792 of yacc.c  */
#line 1013 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 176:
/* Line 1792 of yacc.c  */
#line 1014 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 177:
/* Line 1792 of yacc.c  */
#line 1015 "parser.y"
    {(yyval.child) = (yyvsp[(1) - (1)].child);}
    break;

  case 178:
/* Line 1792 of yacc.c  */
#line 1018 "parser.y"
    {
    if(current_block_ptr->search((yyvsp[(3) - (5)].sym)->get_sym_name(), T_VAR)){
      (yyval.child) = new CopyOf('$' + (yyvsp[(3) - (5)].sym)->get_sym_name());
    }
    else{
      std::cout << (yyvsp[(3) - (5)].sym)->get_sym_name() << " : Symbol not found.\n";
      throw;
    }
}
    break;

  case 179:
/* Line 1792 of yacc.c  */
#line 1027 "parser.y"
    {(yyval.child) = new CopyOf(*(yyvsp[(3) - (5)].str));}
    break;

  case 180:
/* Line 1792 of yacc.c  */
#line 1030 "parser.y"
    { (yyval.child) = new XMLTag((yyvsp[(1) - (1)].sym)->get_sym_name(), true);}
    break;

  case 181:
/* Line 1792 of yacc.c  */
#line 1031 "parser.y"
    { (yyval.child) = new XMLTag((yyvsp[(1) - (1)].sym)->get_sym_name(), false);}
    break;

  case 182:
/* Line 1792 of yacc.c  */
#line 1033 "parser.y"
    {
  (yyval.child) = new Output((yyvsp[(3) - (5)].prop_list_decl));
  }
    break;

  case 183:
/* Line 1792 of yacc.c  */
#line 1039 "parser.y"
    {
  FILE* tmp_file = fopen("tmp.cs", "w");
  if (tmp_file!=NULL)
  {
    fputs (("using System;using System.Collections;using System.Text;using System.Text.RegularExpressions;using System.Xml;using System.Xml.Xsl;using Microsoft.VisualBasic;using System.IO;using System.Xml.XPath;public class Class1{static void Main(){}" + *(yyvsp[(6) - (7)].str) + '}').c_str(),tmp_file);
    fclose(tmp_file);
    FILE* pipe = popen("C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\csc.exe /nologo /r:System.Xml.dll tmp.cs", "r");
    if (!pipe) {std::cout << "ERROR"; return -1;};
    char buffer[128];
    std::string result = "";
    while(!feof(pipe)) {
     if(fgets(buffer, 128, pipe) != NULL)
    	result += buffer;
    }
    pclose(pipe);
    std::cout << result;
    remove("tmp.cs"); //ignore return value
  }
 
 else{
   throw;
 }
  (yyval.child) = new ScriptBlock((yyvsp[(3) - (7)].prop_list_decl), *(yyvsp[(6) - (7)].str));
}
    break;


/* Line 1792 of yacc.c  */
#line 3494 "parser.tab.c"
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }

  yyerror_range[1] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval, &yylloc);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[1] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, yyerror_range, 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


/* Line 2055 of yacc.c  */
#line 1063 "parser.y"


int main(int argc, char *argv[]) {
                // open a file handle to a particular file:
  if(argc < 2){
    cout << "no input file specified.";
    exit(-1);
  }
                FILE *myfile = fopen(argv[1], "r");
                // make sure it is valid:
                if (!myfile) {
                 cout << "can't open the input file." << endl;
                 return -1;
                }
                // set flex to read from it instead of defaulting to STDIN:
                yyin = myfile;

                try{
                // parse through the input until there is no more:
                do {
                  yyparse();
                } while (!feof(yyin));
                }catch(std::exception e){}
}

void yyerror(const char *s) {
                cout << "Error: " << yylloc.first_line - 1 << "  " << yylloc.first_column << ' ' << yylloc.last_line << endl;
                // might as well halt now:
                exit(-1);
}

int yywrap() {return 1; }
