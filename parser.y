/*
    Copyright 2014 abhijeet bhagat

    xDSL is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    xDSL is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with xDSL.  If not, see <http://www.gnu.org/licenses/>.
*/

%{
#include <iostream>
#include <cstdio>
#include "expression.h"
#include <cassert>

using namespace std;

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
 extern int yylineno;
 extern char *yytext;

void yyerror(const char *s);

Block *current_block_ptr = nullptr;
Block *root_ptr = nullptr;
char last_template_child_type;
std::stack<Block*> scopes_stack;
//std::stack<Block*> scopes_stack;
%}

%locations

%union {
  string *str;
  Symbol *sym;
  //StylesheetDeclaration *stylesheet_decl;
  //TemplateDeclaration *temp_decl;
  PropertyDeclaration *prop_decl;
  PropertyDeclarationList *prop_list_decl;
  ArgumentDeclarationList *arg_list_decl;
  WithParamDeclarationList *with_param_list_decl;
  //  BoolExpression *bool_expr;
  RHSValue *rhs_value;
  //IfBlock *if_block;
  IChild *child;
  //ForeachBlock *for_block;
  //CaseBlock *case_block;
  //ElementBlock *element_block;
  Block *block;
}

%token <str> STRING
%token <str> PROPERTY_NAME
%token <str> CODE
%token <str> STRAY
 //%token <expr> INT
%token <sym> SYM
%token <sym> ATTR
%token <sym> HIPHENATED_SYM

%token PLUS
%token MINUS
%token MUL
%token DIVIDE
%token ';'
%token  NL
%token '='
%token SS
%token IN
%token TEMPLATE
%token '(' ')'
%token VAR
%token DOUBLE_QUOTE
%token IF
%token FOR_EACH
%token '{' '}'
%token  ','
%token ':'
%token LOGICAL_OR
%token LOGICAL_AND
%token LOGICAL_COMP
%token CASE
%token WHEN
%token ELSE
%token ELEMENT
%token IMPORT
%token '[' ']'
%token '`'
%token TEXT
%token CALL
%token COPY
%token COPY_OF
%token OUTPUT
%token ARG
%token SCRIPT
%token <sym> START_TAG
%token <sym> END_TAG

%type <child>              stylesheet_decl
%type <block>              stylesheet_children
%type <child>              stylesheet_child
%type <prop_decl>          property_decl
%type <prop_list_decl>     property_comma_seperated_list
%type <prop_list_decl>     template_header
%type <str>                if_start
%type <str>                for_start
%type <str>                when_start 
%type <arg_list_decl>      argument_comma_seperated_list
%type <with_param_list_decl>      with_param_comma_seperated_list
 //%type <rhs_value>          assign
 //%type <bool_expr>          bool_expr
%type <child>              template_decl
%type <child>              temp_child
%type <block>              temp_children
%type <child>              param_block
%type <block>              param_children
%type <child>              param_child
%type <child>              case_block
%type <child>              when_else
%type <child>              when_list
%type <child>              when_clause
%type <block>              when_children
%type <child>              when_child
%type <child>              else_clause
%type <block>              else_children
%type <child>              else_child
%type <child>              if_block
%type <child>              if_child
%type <block>              if_children
%type <child>              for_block
%type <child>              for_child
%type <block>              for_children
%type <child>              element_block
%type <child>              element_child
%type <block>              element_children
%type <child>              attribute_block
%type <child>              attribute_child
%type <block>              attribute_children
%type <child>              straytext
%type <child>              text_node
%type <child>              var_block
%type <block>              var_children
%type <child>              var_child
%type <child>              template_call
%type <child>              copy
%type <block>              copy_children
%type <child>              copy_child
%type <child>              copy_of
%type <block>              arg_decls
%type <child>              arg_decl
%type <block>              arg_children
%type <child>              arg_child
%type <child>              import_decl
%type <child>              xml_start_tag
%type <child>              xml_end_tag
%type <child>              output
%type <child>              script_block

%left PLUS MINUS
%left MUL DIVIDE
%start stylesheet
%%

stylesheet: /*empty*/ {cout << "Nothing to parse\n";}
| SS {
   root_ptr = new Block(nullptr); 
   current_block_ptr = root_ptr;
   scopes_stack.push(current_block_ptr);
  } 
   stylesheet_decl {

  assert($3 != nullptr);
 
cout << $3->render();
 delete root_ptr;
}
;

stylesheet_decl: '{' '}' {$$ = new StylesheetDeclaration(nullptr);}
;

stylesheet_decl: '{' stylesheet_children '}' {
    if(!(root_ptr->all_xml_nodes_ok())){
      std::cout << "no closed tags found\n";
      throw;
    }
    $$ = new StylesheetDeclaration($2);
}
;

stylesheet_children : stylesheet_child
{
  $$ = root_ptr;
  $$->add_child($1);
}
| stylesheet_children stylesheet_child {$$ = $1->add_child($2);}
;

stylesheet_child :
  param_block   {$$ = $1;}
| var_block     {$$ = $1;}
| template_decl {$$ = $1;}
| import_decl   {$$ = $1;}
| straytext     {$$ = $1;}
| output        {$$ = $1;}
| script_block  {$$ = $1;} 
| xml_start_tag {$$ = $1; current_block_ptr->push_on_stack(static_cast<XMLTag*>($1)->get_tag_name());}
| xml_end_tag   {
 if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>($1)->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>($1)->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
 }
 $$ = $1;
}
;

import_decl : IMPORT STRING ';' {$$ = new ImportDeclaration(*$2);}
;

param_block:
IN SYM '=' SYM ';'
{

  if(!current_block_ptr->search_current_scope($2->get_sym_name(), T_VAR) && current_block_ptr->search($4->get_sym_name(), T_VAR)){
      current_block_ptr->add_symbol($2->get_sym_name(), T_VAR);
      $$ = new ParamBlock($2->get_sym_name(), "$"+$4->get_sym_name());
    }
    else{
      std::cout << $4->get_sym_name() << " : Symbol not found.\n";
      throw;
    }
}

| IN SYM '=' STRING ';'
{
  if(!current_block_ptr->search_current_scope($2->get_sym_name(), T_VAR)){
   current_block_ptr->add_symbol($2->get_sym_name(), T_VAR);
   $$ = new ParamBlock($2->get_sym_name(), *$4);
  }
  else{
   std::cout << $2->get_sym_name() << " : Symbol redeclared.\n";
   throw;
  }
}
| IN SYM ';'
{
  if(!current_block_ptr->search_current_scope($2->get_sym_name(), T_VAR)){
   current_block_ptr->add_symbol($2->get_sym_name(), T_VAR);
   $$ = new ParamBlock($2->get_sym_name(), "''");
  }
  else{
   std::cout << $2->get_sym_name() << " : Symbol redeclared.\n";
   throw;
  }
}
| IN SYM '{'
 {
  Block *b = current_block_ptr;
  current_block_ptr = new Block(nullptr);
  current_block_ptr->add_parent(b);
  scopes_stack.push(current_block_ptr);
 }
 param_children '}'
{
  //Process and remove the top element from stack
  //current_block_ptr = scopes_stack.top();
  //scopes_stack.pop();

  if(!current_block_ptr->search_current_scope($2->get_sym_name(), T_VAR)){
    current_block_ptr->add_symbol($2->get_sym_name(), T_VAR);
   $$ = new ParamBlock($2->get_sym_name(), $5);
   
   scopes_stack.pop();
   current_block_ptr = scopes_stack.top();
  }
  else{
   std::cout << $2->get_sym_name() << " : Symbol redeclared.\n";
   throw;
  }
}
;

param_children :
param_child
{
  //scopes_stack.push(current_block_ptr); //save the current scope
  //$$ = new Block($1);
  //$$->add_parent(current_block_ptr);
  //current_block_ptr = $$;
  $$ = current_block_ptr->add_child($1);
}
| param_children param_child {$$ = $1->add_child($2);}
;

param_child:
var_block     {$$ = $1;}
| if_block      {$$ = $1;}
| for_block     {$$ = $1;}
| case_block    {$$ = $1;}
| element_block {$$ = $1;}
| text_node     {$$ = $1;}
| xml_start_tag {$$ = $1;  current_block_ptr->push_on_stack(static_cast<XMLTag*>($1)->get_tag_name());}
| xml_end_tag {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>($1)->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>($1)->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    $$ = $1;
  }
;

var_block:
VAR SYM '=' SYM ';'
{

  if(!current_block_ptr->search_current_scope($2->get_sym_name(), T_VAR) /*&& current_block_ptr->search($4->get_sym_name(), T_VAR)*/){
      current_block_ptr->add_symbol($2->get_sym_name(), T_VAR);
      $$ = new VariableBlock($2->get_sym_name(), "$"+$4->get_sym_name(), current_block_ptr);
    }
    else{
      std::cout << $4->get_sym_name() << " : Symbol not found.\n";
      throw;
    }
}

| VAR SYM '=' STRING ';'
{
  if(!current_block_ptr->search_current_scope($2->get_sym_name(), T_VAR)){
   current_block_ptr->add_symbol($2->get_sym_name(), T_VAR);
   $$ = new VariableBlock($2->get_sym_name(), *$4);
  }
  else{
   std::cout << $2->get_sym_name() << " : Symbol redeclared.\n";
   throw;
  }
}
| VAR SYM ';'
{
  if(!current_block_ptr->search_current_scope($2->get_sym_name(), T_VAR)){
   current_block_ptr->add_symbol($2->get_sym_name(), T_VAR);
   $$ = new VariableBlock($2->get_sym_name(), "''");
  }
  else{
   std::cout << $2->get_sym_name() << " : Symbol redeclared.\n";
   throw;
  }
}
| VAR SYM 
 '{' {
  Block *b = current_block_ptr;
  current_block_ptr = new Block(nullptr);
  current_block_ptr->add_parent(b);
  scopes_stack.push(current_block_ptr);
  } var_children '}'
{
  //Process and remove the top element from stack
  //current_block_ptr = scopes_stack.top();
  //scopes_stack.pop();

  if(!current_block_ptr->search_current_scope($2->get_sym_name(), T_VAR)){
    current_block_ptr->add_symbol($2->get_sym_name(), T_VAR);
   $$ = new VariableBlock($2->get_sym_name(), $5);
   scopes_stack.pop();
   current_block_ptr = scopes_stack.top();
  }
  else{
   std::cout << $2->get_sym_name() << " : Symbol redeclared.\n";
   throw;
  }
}
;

var_children :
var_child
{
  //scopes_stack.push(current_block_ptr); //save the current scope
  // $$ = new Block($1);
  //$$->add_parent(current_block_ptr);
  //current_block_ptr = $$;
  $$ = current_block_ptr->add_child($1);
}
| var_children var_child {$$ = $1->add_child($2);}
;

var_child:
var_block     {$$ = $1;}
| if_block      {$$ = $1;}
| for_block     {$$ = $1;}
| case_block    {$$ = $1;}
| element_block {$$ = $1;}
| text_node     {$$ = $1;}
| xml_start_tag {$$ = $1;  current_block_ptr->push_on_stack(static_cast<XMLTag*>($1)->get_tag_name());}
| xml_end_tag {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>($1)->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>($1)->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    $$ = $1;
  }
;

template_header: TEMPLATE '[' property_comma_seperated_list ']'
{
  for(auto b = $3->begin(); b != $3->end(); ++b){
                  if(b->first == "name"){
                                    try{
                     root_ptr->add_symbol(b->second, T_TEMPLATE);
                                    }catch(SymbolExistsException e){
                                        std::cout << b->second << ':' << e.what() << '\n';
                        throw e;
                                    }
                  }
                }
  $$ = $3;
  //Block* ptr = current_block_ptr;
  current_block_ptr = new Block(nullptr);
  current_block_ptr->add_parent(root_ptr);
  scopes_stack.push(current_block_ptr);
}
;

template_decl:
template_header  '{'               '}'  {
    try{
      $$ = new TemplateDeclaration($1, nullptr, nullptr);
    }catch(InvalidPropertyException e){
      std::cout << e.what();
      throw;
    }
}
|

template_header '{' temp_children '}'
{
  try{
    if(!($3->all_xml_nodes_ok())){
      std::cout << "no closed tags found\n";
      throw;
    }
    $$ = new TemplateDeclaration($1, nullptr, $3);
    //pop the current scope as we are done with it
    scopes_stack.pop();
    current_block_ptr = scopes_stack.top();
  }catch(InvalidPropertyException e){
    std::cout << e.what();
    throw;
  }
}
;

|

template_header '(' argument_comma_seperated_list ')' '{''}'
{
  try{
    $$ = new TemplateDeclaration($1, $3, nullptr);
  }catch(InvalidPropertyException e){
    std::cout << e.what();
    throw;
  }
}
;

|

template_header '(' argument_comma_seperated_list ')' '{' temp_children '}'
{
  try{
    $$ = new TemplateDeclaration($1, $3, $6);
  }catch(InvalidPropertyException e){
    std::cout << e.what();
    throw;
  }
}
;

temp_children :
temp_child
{
  $$ = current_block_ptr->add_child($1);//current_block_ptr;//new Block($1);
  //$$->add_parent(current_block_ptr);
  //current_block_ptr = $$;
  if(ParamBlock *p = dynamic_cast<ParamBlock*>($1))
    last_template_child_type = 'p';
  else
    last_template_child_type = ' ';
}

|temp_children temp_child
{
  if(ParamBlock *p = dynamic_cast<ParamBlock*>($2)){
    if(!(last_template_child_type == 'p')){
     std::cout << "A param (if any) should be the first element in a template definition\n";
     throw;
    }
    else{
      $$ = $1->add_child($2);
    }
  }
  else{
    $$ = $1->add_child($2);
    last_template_child_type = ' ';
  }
  /*  if($1->get_children_count() >= 0 && (p = dynamic_cast<ParamBlock*>($2))){
    std::cout << "A param (if any) should be the first element in a template definition\n";
    throw;
  }
  $$ = $1->add_child($2);*/
}
;

temp_child:
  param_block   {$$ = $1;}
| var_block     {$$ = $1;}
| if_block      {$$ = $1;}
| for_block     {$$ = $1;}
| case_block    {$$ = $1;}
| element_block {$$ = $1;}
| text_node     {$$ = $1;}
| copy          {$$ = $1;}
| copy_of       {$$ = $1;}
| template_call {$$ = $1;}
| when_else     {$$ = $1;} 
| straytext     {$$ = $1;}
| xml_start_tag {$$ = $1;  current_block_ptr->push_on_stack(static_cast<XMLTag*>($1)->get_tag_name());}
| xml_end_tag {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>($1)->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>($1)->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    $$ = $1;
  }
;

property_comma_seperated_list : property_decl {$$ = new PropertyDeclarationList($1); }
| property_comma_seperated_list ',' property_decl {$$ = $1->add_property($3);}
;

property_decl:
SYM ':' SYM {$$ = new PropertyDeclaration($1->get_sym_name(), $3->get_sym_name());}
|HIPHENATED_SYM ':' SYM {$$ = new PropertyDeclaration($1->get_sym_name(), $3->get_sym_name());}
|SYM ':' STRING {$$ = new PropertyDeclaration($1->get_sym_name(), *$3);}
;

argument_comma_seperated_list : property_decl {$$ = new ArgumentDeclarationList($1);}
| argument_comma_seperated_list ',' property_decl {$$ = reinterpret_cast<ArgumentDeclarationList*>($1->add_property($3));}
;

with_param_comma_seperated_list : property_decl {$$ = new WithParamDeclarationList($1);}
| with_param_comma_seperated_list ',' property_decl {$$ = reinterpret_cast<WithParamDeclarationList*>($1->add_property($3));}
;

/*if_start : IF
{
  $<block>$ = new Block(nullptr);

  $<block>$->add_parent(current_block_ptr);
  current_block_ptr = $<block>$;
}
;
*/

if_start : IF '(' STRING ')'
{
  $$ = $3;
  //Block* ptr = current_block_ptr;
  Block* block = current_block_ptr; //save parent block ptr
  current_block_ptr = new Block(nullptr); //create a new block for if
  current_block_ptr->add_parent(block); //add parent to the if block
  scopes_stack.push(current_block_ptr);
}
;
if_block :
if_start /* '(' STRING ')'*/ '{'            '}' {$$ = new IfBlock(*$1, nullptr);}
| if_start /* '(' STRING ')'*/ '{' if_children '}' {
   if(!(current_block_ptr->all_xml_nodes_ok())){
      std::cout << "no closed tags found\n";
      throw;
   }
   $$ = new IfBlock(*$1, $3);
   scopes_stack.pop();
   current_block_ptr = scopes_stack.top();
  }
| if_start /*'(' STRING ')'*/ '{'             '}' else_clause
{
  Block *block = new Block(new WhenBlock(*$1, nullptr));
  block->add_child($4);
  $$ = new CaseBlock(block) ;
}
| if_start /*'(' STRING ')'*/ '{' if_children '}' else_clause
{
  Block *block = new Block(new WhenBlock(*$1, $3));
  block->add_child($5);
  $$ = new CaseBlock(block) ;
  scopes_stack.pop();
  current_block_ptr = scopes_stack.top();
}
;

if_children: if_child
{
  //$$ = new Block($1);
  //current_block_ptr is already pointing to a new Block.
  $$ = current_block_ptr->add_child($1);//current_block_ptr;
  //$$->add_parent(current_block_ptr);

  //current_block_ptr = $$;

}
|if_children if_child {$$ = $1->add_child($2);}
;

if_child :
  var_block   {$$ = $1;}
| for_block {$$ = $1;}
| case_block {$$ = $1;}
| if_block {$$ = $1;}
| template_call {$$ = $1;}
| xml_start_tag {$$ = $1;  current_block_ptr->push_on_stack(static_cast<XMLTag*>($1)->get_tag_name());}
| xml_end_tag {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>($1)->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>($1)->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    $$ = $1;
  }
;

/*bool_expr: bool_expr LOGICAL_AND bool_expr {$$ = new BoolExpression($1->render() + "&&" + $3->render());}
| bool_expr LOGICAL_OR bool_expr
| bool_expr '=' bool_expr
| SYM {$$ = new BoolExpression($1->get_sym_name());}
;*/

for_start :   FOR_EACH '(' STRING ')'
{
 $$ = $3;
  //Block* ptr = current_block_ptr;
  Block* block = current_block_ptr; //save parent block ptr
  current_block_ptr = new Block(nullptr); //create a new block for for
  current_block_ptr->add_parent(block); //add parent to the for block
  scopes_stack.push(current_block_ptr);
}
;

for_block :
  for_start '{'              '}' {$$ = new ForeachBlock(*$1, nullptr); }
| for_start '{' for_children '}' {
   if(!(current_block_ptr->all_xml_nodes_ok())){
      std::cout << "no closed tags found\n";
      throw;
   }
   $$ = new ForeachBlock(*$1, $3); 
   scopes_stack.pop();
   current_block_ptr = scopes_stack.top();
}
;

for_children : for_child {
   //current_block_ptr is already pointing to a new Block.
  $$ = current_block_ptr->add_child($1);
}
| for_children for_child {$$ = $1->add_child($2);}

for_child :
  var_block   {$$ = $1;}
| for_block {$$ = $1;}
| case_block {$$ = $1;}
| if_block {$$ = $1;}
| xml_start_tag {$$ = $1;  current_block_ptr->push_on_stack(static_cast<XMLTag*>($1)->get_tag_name());}
| xml_end_tag {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>($1)->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>($1)->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    $$ = $1;
  }
;


case_block : CASE '{' when_list else_clause '}'
 {
   Block *b = nullptr;
   if($3 != nullptr){
     b = static_cast<Block*>($3)->add_child($4);
   }
   $$ = new CaseBlock(b);
 }
;

when_else : 

when_clause else_clause
{

  Block *b = new Block($1);
  b->add_child($2);

  $$ = new CaseBlock(b);
}
;

when_list : when_clause {$$ = new Block($1);}
| when_list when_clause {$$ = static_cast<Block*>($1)->add_child($2);}
;

when_start : WHEN '(' STRING ')'
{
 $$ = $3;
  //Block* ptr = current_block_ptr;
  Block* block = current_block_ptr; //save parent block ptr
  current_block_ptr = new Block(nullptr); //create a new block for for
  current_block_ptr->add_parent(block); //add parent to the for block
  scopes_stack.push(current_block_ptr);
}

when_clause: when_start '{' '}' {$$ = new WhenBlock(*$1, nullptr);}
;

when_clause: when_start '{' when_children '}' {
  if(!(current_block_ptr->all_xml_nodes_ok())){
      std::cout << "no closed tags found\n";
      throw;
   }
  $$ = new WhenBlock(*$1, static_cast<Block*>($3));
  scopes_stack.pop();
  current_block_ptr = scopes_stack.top();
}
  
;

when_children : when_child {  
  //current_block_ptr is already pointing to a new Block.
  $$ = current_block_ptr->add_child($1);
}
| when_children when_child {$$ = $1->add_child($2);}

when_child:
  var_block   {$$ = $1;}
| if_block {$$ = $1;}
| for_block {$$ = $1;}
| case_block {$$ = $1;}
| xml_start_tag {$$ = $1;  current_block_ptr->push_on_stack(static_cast<XMLTag*>($1)->get_tag_name());}
| xml_end_tag {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>($1)->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>($1)->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    $$ = $1;
  } 
;

else_start : ELSE
{
  //Block* ptr = current_block_ptr;
  Block* block = current_block_ptr; //save parent block ptr
  current_block_ptr = new Block(nullptr); //create a new block for for
  current_block_ptr->add_parent(block); //add parent to the for block
  scopes_stack.push(current_block_ptr);
}
//TODO: Remove the reduce/reduce conflict because of the empty production
else_clause : /*empty*/{$$ = nullptr;};

else_clause : else_start '{' '}' {$$ = new OtherwiseBlock(nullptr);}
;

else_clause : else_start '{' else_children '}' {
  if(!(current_block_ptr->all_xml_nodes_ok())){
      std::cout << "no closed tags found\n";
      throw;
   }
  $$ = new OtherwiseBlock(static_cast<Block*>($3));
  scopes_stack.pop();
  current_block_ptr = scopes_stack.top();
}
;

else_children : else_child {  
  //current_block_ptr is already pointing to a new Block.
  $$ = current_block_ptr->add_child($1);
}
| else_children else_child {$$ = $1->add_child($2);}

else_child:
  var_block   {$$ = $1;}
| if_block {$$ = $1;}
| for_block {$$ = $1;}
| case_block {$$ = $1;}
| xml_start_tag {$$ = $1;  current_block_ptr->push_on_stack(static_cast<XMLTag*>($1)->get_tag_name());}
| xml_end_tag {
  if(current_block_ptr->all_xml_nodes_ok()){
    std::cout << static_cast<XMLTag*>($1)->render() << ": no starting tag found\n";
    throw;
  }else{
    auto s = current_block_ptr->pop_from_stack();
    if(s != "" && (s != static_cast<XMLTag*>($1)->get_tag_name())){
      std::cout << "tag mismatch\n";
      throw XMLTagMismatchedException();
    }
  }

    $$ = $1;
  } 
;

element_block :
ELEMENT '[' property_comma_seperated_list ']' '(' property_comma_seperated_list ')' '{' '}' {$$ = new ElementBlock($3, $6, nullptr);}
| ELEMENT '[' property_comma_seperated_list ']' '(' property_comma_seperated_list ')' '{' element_children '}'
{
  $$ = new ElementBlock($3, $6, $9);
}
| ELEMENT '[' property_comma_seperated_list ']' '{' element_children '}'
{
  $$ = new ElementBlock($3, nullptr, $6);
}
| ELEMENT '[' property_comma_seperated_list ']' '{'  '}'
{
  $$ = new ElementBlock($3, nullptr, nullptr);
}
;

element_children:
element_child
{
  $$ = new Block($1);
  $$->add_parent(current_block_ptr);
  current_block_ptr = $$;
}
| element_children element_child {$$ = $1->add_child($2);}
;

element_child:
  var_block {$$ = $1;}
| if_block {$$ = $1;}
| for_block {$$ = $1;}
| case_block {$$ = $1;}
| attribute_block {$$ = $1;}
;

attribute_block: ATTR '{' attribute_children '}' {$$ = new Attribute($1->get_sym_name(), $3);}
;

attribute_children :
attribute_child
{
  $$ = new Block($1);
  $$->add_parent(current_block_ptr);
  current_block_ptr = $$;
}
| attribute_children attribute_child {$$ = $1->add_child($2);}
;

attribute_child:
var_block  {$$ = $1;}
|if_block  {$$ = $1;}
|for_block  {$$ = $1;}
|case_block  {$$ = $1;}
|straytext   {$$ = $1;}
;

straytext : STRAY {$$ = new StrayText(*$1);}
;

text_node : TEXT '=' straytext ';' {$$ = new Text(false, $3->render());}
| TEXT ';' {$$ = new Text(false, "");}
| TEXT '[' property_decl ']' '=' straytext ';'
{
  if($3->get_key() == "disable-output-escaping" && ($3->get_value() == "yes" || $3->get_value() == "no")){
   $$ = new Text(($3->get_value() == "yes" ? true : false), $6->render());
 }
 else{
   throw InvalidPropertyException();
 }
}
;

template_call :
CALL SYM '(' with_param_comma_seperated_list ')' ';'
{
  $$ = new TemplateCall($2->get_sym_name(), $4, root_ptr, nullptr);
}
|CALL SYM '(' ')' ';'
{
  $$ = new TemplateCall($2->get_sym_name(), nullptr, root_ptr, nullptr);
}
|CALL SYM '{' arg_decls '}'
{
  $$ = new TemplateCall($2->get_sym_name(), nullptr, root_ptr, $4);
}
;

arg_decls : arg_decl
{
  $$ = new Block($1);
  $$->add_parent(current_block_ptr);
  current_block_ptr = $$;
  //scopes_stack.push($$); //push the current block (scope) on the stack
}
| arg_decls arg_decl {$$ = $1->add_child($2);}
;

arg_decl :
ARG '[' SYM ']' '{' arg_children '}'
{
  //current_block_ptr should point to the arg_decls block when validating the arg-name (SYM)
  //scopes_stack.pop();
  //current_block_ptr = scopes_stack.top();
  if(!current_block_ptr->search($3->get_sym_name(), T_PARAM)){
    $$ = new WithParamBlock($3->get_sym_name(), $6);
  }
  else{
    std::cout << $3->get_sym_name() << " : Symbol not found.\n";
    throw;
  }
}
;

arg_children : arg_child
{
  //  scopes_stack.push(current_block_ptr); //save the current scope for later validations
  $$ = new Block($1);
  $$->add_parent(current_block_ptr);
  current_block_ptr = $$;
}
|arg_children arg_child {$$ = $1->add_child($2);}
;

arg_child:
  var_block {$$ = $1;}
| if_block {$$ = $1;}
| for_block {$$ = $1;}
| case_block {$$ = $1;}
| attribute_block {$$ = $1;}
;

copy :
COPY '[' property_decl ']' '{' '}' {$$ = new Copy($3, nullptr);}
|COPY '[' property_decl ']' '{' copy_children '}' {$$ = new Copy($3, $6);}
|COPY '{' '}' {$$ = new Copy(nullptr, nullptr);}
|COPY '{' copy_children '}' {$$ = new Copy(nullptr, $3);}
;

copy_children : copy_child
{
    $$ = new Block($1);
  $$->add_parent(current_block_ptr);
  current_block_ptr = $$;
}
|copy_children copy_child {$$ = $1->add_child($2);}
;

copy_child:
  var_block {$$ = $1;}
| if_block {$$ = $1;}
| for_block {$$ = $1;}
| case_block {$$ = $1;}
| attribute_block {$$ = $1;}
;

copy_of : COPY_OF '(' SYM ')' ';'{
    if(current_block_ptr->search($3->get_sym_name(), T_VAR)){
      $$ = new CopyOf('$' + $3->get_sym_name());
    }
    else{
      std::cout << $3->get_sym_name() << " : Symbol not found.\n";
      throw;
    }
}
|COPY_OF '(' STRING ')' ';' {$$ = new CopyOf(*$3);}
;

xml_start_tag : START_TAG { $$ = new XMLTag($1->get_sym_name(), true);}
xml_end_tag : END_TAG { $$ = new XMLTag($1->get_sym_name(), false);}

output : OUTPUT '[' property_comma_seperated_list ']' ';'{
  $$ = new Output($3);
  }
;

script_block : SCRIPT '[' property_comma_seperated_list ']' '{' CODE '}'
{
  FILE* tmp_file = fopen("tmp.cs", "w");
  if (tmp_file!=NULL)
  {
    fputs (("using System;using System.Collections;using System.Text;using System.Text.RegularExpressions;using System.Xml;using System.Xml.Xsl;using Microsoft.VisualBasic;using System.IO;using System.Xml.XPath;public class Class1{static void Main(){}" + *$6 + '}').c_str(),tmp_file);
    fclose(tmp_file);
    FILE* pipe = popen("C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\csc.exe /nologo /r:System.Xml.dll tmp.cs", "r");
    if (!pipe) {std::cout << "ERROR"; return -1;};
    char buffer[128];
    std::string result = "";
    while(!feof(pipe)) {
     if(fgets(buffer, 128, pipe) != NULL)
    	result += buffer;
    }
    pclose(pipe);
    std::cout << result;
    remove("tmp.cs"); //ignore return value
  }
 
 else{
   throw;
 }
  $$ = new ScriptBlock($3, *$6);
};
%%

int main(int argc, char *argv[]) {
                // open a file handle to a particular file:
  if(argc < 2){
    cout << "no input file specified.";
    exit(-1);
  }
                FILE *myfile = fopen(argv[1], "r");
                // make sure it is valid:
                if (!myfile) {
                 cout << "can't open the input file." << endl;
                 return -1;
                }
                // set flex to read from it instead of defaulting to STDIN:
                yyin = myfile;

                try{
                // parse through the input until there is no more:
                do {
                  yyparse();
                } while (!feof(yyin));
                }catch(std::exception e){}
}

void yyerror(const char *s) {
                cout << "Error: " << yylloc.first_line - 1 << "  " << yylloc.first_column << ' ' << yylloc.last_line << endl;
                // might as well halt now:
                exit(-1);
}

int yywrap() {return 1; }
