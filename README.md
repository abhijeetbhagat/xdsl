![Alt xDSL](https://bitbucket.org/abhijeetbhagat/xdsl/raw/fd247afcf5f4b270a356896595ecdbd389797e33/favicon.ico)

xDSL is a language that translates to XSLT.
For eg. The following code -

```
ss{
 in d;	

 var asdad = "";
 var jjh = asdad;
 var abc = "";
 var empty; 
 var block{
  if(""){
  }
 }

 template[name:t]{
}

 template[name:test](par:p){
   if(""){
    call test(par:d);
   }  

   elem[name:test](name:abc, attack:wilshere, match:somematch){
    @venue{`emirates`;}
   }

   elem[name:test]{
    @venue{`emirates`;}
   }
   
   text = `txt`;
   text[disable-output-escaping : yes] = `txt`;
   text;
 }
}

```

translates to this -

```
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="d" select="''" />
  <xsl:variable name="asdad" select="''" />
  <xsl:variable name="jjh" select="$asdad" />
  <xsl:variable name="abc" select="''" />
  <xsl:variable name="empty" select="''" />
  <xsl:variable name="block">
    <xsl:if test="''">
    </xsl:if>
  </xsl:variable>
  <xsl:template name="t" >
  </xsl:template>
  <xsl:template name="test" >
    <xsl:param name="par" select="p" />
    <xsl:if test="''">
      <xsl:call-template name="test">
        <xsl:with-param name="par" select="d" />
      </xsl:call-template>
    </xsl:if>
    <xsl:element name="test" >
      <xsl:attribute name="name">abc</xsl:attribute>
      <xsl:attribute name="attack">wilshere</xsl:attribute>
      <xsl:attribute name="match">somematch</xsl:attribute>
      <xsl:attribute name="venue">emirates</xsl:attribute>
    </xsl:element>
    <xsl:element name="test" >
      <xsl:attribute name="venue">emirates</xsl:attribute>
    </xsl:element>
    <xsl:text>txt</xsl:text>
    <xsl:text disable-output-escaping = "yes">txt</xsl:text>
    <xsl:text></xsl:text>
  </xsl:template>
</xsl:stylesheet>
```

Run the make file as:
    `$ make xDSL`

Run the translator as:
    `$ xDSL `

Enable debugging:
	`$ make CFLAGS=-g xDSL`
